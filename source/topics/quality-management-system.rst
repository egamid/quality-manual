
-------------------------
Quality management system
-------------------------

General Requirements
--------------------

Mythical True Value (MTV) Metrology has established, documented and implements an effective QMS as a means of ensuring that its calibration services conform to specified requirements, to foster an environment of continual improvement with an eventual goal of achieving laboratory accreditation to ISO/IEC 17025, and to identify its position as a leading metrology laboratory in the aviation maintenance and repair organization community.

The department’s QMS is comprised of the Core and Support Business Processes that take market opportunities and converts them into value added outputs to satisfy our customers’ needs and expectations.

**Core Processes:**

The core processes describe all the processes that are necessary for the department to realize and deliver the desired service to its customers. QP 4100 is an overview of the processes and their interactions. The Core Processes for the department are listed below:

   Receiving Process (QP 4101)

   Evaluation Process (QP 4102)

   Calibration Process (QP 4103)

   Repair Process (QP 4104)

   Process for control of Outside Services (QP 4105)

   Out-Processing Process (QP 4106)

**Support Processes:**

Mythical True Value Metrology Support Processes describe all other business requirements that are necessary to manage and control resources, and to conduct business in an orderly manner. The support processes are implemented and managed in accordance with the applicable requirements of the International Standard. They include:

   Quality Management (Business Operating Manual)

   Document Management (QP 4230, QP 4240)

   Facility and Equipment Management (QP 6300, QP 6400)

   Information Technology Management (QP 6301)

   Measurement Standards (QP 7600)

   Human Resources (QP 6220)

The sequence and interaction of the Core and Support Business Processes are described in QMS Planning and QP 4100.

The criteria and methods required to ensure the effective operation and control of these processes are defined and documented.

The entire QMS documentation and other key information necessary to support the operation and monitoring of the department business processes are available to all department employees, and other parties requiring access, on the Laboratory network.

Appropriate levels and types of monitoring and measurement of core and supplemental processes have been determined and are documented in relevant policies and procedures.

The QMS includes policies and procedures for implementation of actions required to achieved planned results, and for continual improvement of the business processes.

Documentation Requirements
--------------------------

Mythical True Value Metrology has defined and documented quality procedures consistent with the requirements of the standard. The quality procedures further describe criteria, methods, detail activities, responsibilities and the quality assurance measures that are required to ensure the effective operation and control of the department business processes. The department QMS also includes other documents and records required by the organization to ensure the effective operation and control of the business processes. The degree of documentation is consistent with:

-  the methods used,

-  the complexity and interaction of the business processes, and

-  skills needed and training required by the personnel involved in carrying out these activities.

An outline of the department QMS documentation is shown in :numref:`Fig. {number} <documentation>`

.. figure:: figures/documentation.png
    :width: 500px
    :name: documentation
    
    Documentation
    
The QMS is maintained, updated, and continually improved as the department seeks better business practices. Mythical True Value Metrology employees have been trained on:

-  which procedures apply to them,

-  how to access those procedures,

-  how to apply them to their job function, and

-  how to report related results.

Business Operating Manual
~~~~~~~~~~~~~~~~~~~~~~~~~

A Business Operating Manual (this document, also known as the quality manual) has been established and maintained and includes:

-  the scope of the QMS, including details of and justification for any exclusions;

-  documented procedures or reference to them; and
    
-  a description of the sequence and interaction of the processes included in the QMS.

The Business Operating Manual is a controlled document subject to the requirements of :ref:`Control of Documents`

Control of Documents 
~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology ensures that pertinent issues of documents and data that relate to the requirements of the QMS are controlled. This control also extends to documents of external origin that are maintained solely by Mythical True Value Metrology and not by other departments of Mythical Airlines, such as customer drawings or requirements, performance standards, and service specifications. This policy is implemented by QP 4230.

Document and Data Approval and Issue: 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All documents and data that relate to the requirements of the Business Management System and the Standard are reviewed and approved for suitability by authorized personnel prior to use or release. Where use is made of computer-based documents and files, special attention is paid to appropriate approval, access, distribution, and archiving procedures.

Currently Mythical True Value Metrology has a mixed media documentation system for documents within its control:

-  Levels 1 and 2 are computer based, and are available as read-only documents.

-  Level 3 documents (such as work instructions, standard operating procedures, or calibration procedures) are also in electronic format wherever possible.

-  For the remaining Level 3 documents not in electronic format, authorized distribution to points of use is in hard copy. This includes many documents of external origin.

All electronic QMS documents are stored in protected folders. Many are further protected from unauthorized change by use of digital signatures that implement the RSA public key encryption algorithm. Update access privileges to these folders are limited to the designated document control administrator.

A list of documents, identifying the current revision status of each, is established and is readily available. Appropriate documents are available at all locations where operations essential to the effective functioning of the Business Management System are performed, or they may be readily obtained.

Document and Data Changes 
^^^^^^^^^^^^^^^^^^^^^^^^^

All personnel are required to identify required changes in documents and are encouraged to suggest improvements. Changes to documents and data are reviewed and approved by the same function/organizations that performed the original review and approval, unless specifically designated otherwise. Access to appropriate background information is provided. The nature of the changes is recorded.

When practical, the changes are highlighted in the document and/or on attachments.

Obsolete Documents
^^^^^^^^^^^^^^^^^^

Electronically maintained documents are promptly removed from active files when they become obsolete. They may be maintained for the document retention period, in file areas not accessible to general users.

After a revision issue, obsolete hard copy documents are promptly removed from points of issue and use. The obsolete master document is archived and may be retained for legal and/or knowledge preservation purposes. The obsolete point of use copy is destroyed.

.. note::

    Due to the nature of work performed by Mythical True Value Metrology (calibration and repair of precision tools, inspection, measuring and test equipment) there may be multiple versions of Level 3 documents for a given manufacturer/model of equipment. This is an acceptable situation because: it is due to version changes during production by the manufacturer of the equipment being calibrated and is outside the control of Mythical True Value Metrology; and the metrology technicians are responsible for selecting the correct document version based on the specific version or serial number of the item being calibrated.

    These documents are also exempt from the requirement to destroy old copies, unless and until it can be demonstrated that there will never again be a requirement to calibrate or repair that specific instrument from any current or potential future customer. The primary reason for this exemption is that some of the workload items may be several decades beyond their manufacturer's support life and the data owned by Mythical True Value Metrology may well be the only data available anywhere.

Documents of External Origin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Due to the nature of the organization as a department of a large corporation, there are a number of documents of external origin that we must use and follow but which are controlled elsewhere. Access to and use of these documents is in accordance with applicable Mythical Airlines policies.

Mythical True Value Metrology is also subject to various government regulations. When there is a need to refer to any of these regulations, access is gained through the Mythical Airlines system or the regulatory agency system. Our presumption is that the link from that site goes to the most current edition of the regulation. Any other copies of these regulations are not authorized for use.

These and other situations are further documented in QP 4230.

Control of Quality Records
~~~~~~~~~~~~~~~~~~~~~~~~~~

Quality records are a special case of quality documents. For example, a blank form is a document, a completed form is a record.

Mythical True Value Metrology maintains quality records to demonstrate conformance to specified requirements and the effective operation of the Business Management System. Control provides for the identification, collection, indexing, filing, access, storage, maintenance, and disposition of quality records. Pertinent customer or supplier records are an element of these data.

All quality records are legible and are stored and retained in such a manner that they are readily retrievable. Storage facilities provide an environment that minimizes damage, deterioration and prevents loss. Retention times of quality records are established and recorded. A records control matrix is documented to aid control. QP 4240 is established to implement the above policies and to control Business Management System records.

