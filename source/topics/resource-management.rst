
-------------------
Resource management
-------------------

Provision of Resources
----------------------

Top management of Mythical True Value Metrology determines and provides, in a timely manner, the resources needed:

-  to implement, maintain and improve the effective operations of the QMS processes, and

-  to enhance customer satisfaction by meeting requirements.

These resources are assessed and reviewed on a periodic basis consistent with annual and strategic business planning activities.

Human Resources
---------------

Assignment of Personnel
~~~~~~~~~~~~~~~~~~~~~~~

Personnel who are assigned responsibilities directly or indirectly affecting conformity to service (product) requirements are determined to be qualified and competent based on education, training, observed skills, and experience.

Competence, Awareness, and Training
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies training needs and provides for the training of all personnel performing activities affecting quality. Personnel performing specific assigned tasks are qualified on the basis of appropriate education, training, observed skills, and experience, as required. All metrology technicians must be evaluated as fully competent in a discipline of metrology before being allowed to work independently in that discipline.

A training plan is prepared and scheduled annually with periodic updates.

Training is provided either on or off the job, internally or externally, as appropriate. The ongoing effectiveness of training is periodically assessed by observation, proficiency testing or other appropriate means.

Mythical True Value Metrology ensures that its personnel are aware of the relevance and importance of their jobs and how they contribute to the achievement of the quality objectives.

Mythical True Value Metrology securely retains duplicate copies of training records in the form of attendance sheets, certificates of proficiency and/or completion for all employees. Original copies of these records are maintained by Mythical Airlines Human Resources Department, which is outside the scope of this quality management system.

Quality Procedure QP 6220 implements the above policies.

Infrastructure
--------------

Top management of Mythical True Value Metrology identifies, provides and maintains the facilities and infrastructure it needs to achieve service quality, including:

-  workspace and associated facilities, including utilities;

-  process equipment, hardware and software;

-  supporting services such as telephone, computer networks, etc.

QP 6300 and QP 6301 have been established to further describe and implement the above policies.

Work Environment 
----------------

Top management of Mythical True Value Metrology identifies and manages the human and physical factors of the work environment needed to achieve conformity of service quality.

QP 6400 has been established to further describe and manage the above policies, and to define the physical environmental requirements for each metrology discipline.