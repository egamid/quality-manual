
-------------------
Service realization
-------------------

.. note:: The product of Mythical True Value Metrology is the service of providing calibration of tools, precision measuring equipment, and testing equipment.

Planning of Service Realization 
-------------------------------

Upon receipt of new service requirements, or significant changes (modify, add, remove) to existing requirements, Mythical True Value Metrology management plans the sequence of processes required to realize the specified requirements. Planning the processes for the realization of service quality includes but is not limited to the functions listed below.

-  Prepare documentation and data that describes how the processes of the QMS are applied to a specific service. These are referred to as calibration procedures or calibration documents. Such documents describe the quality objectives and requirements, the sequence of processes and sub-processes required to achieve the contracted services including specified performance objectives. Quality plans are consistent with the other requirements of the organization's QMS and are documented in a form suitable for the department’s method of operation.

-  Identify and document suitable verification, validation, monitoring, process measurement, inspection, and testing at appropriate stages of the calibration service realization.

-  Identify and acquire controls, processes, equipment, fixtures, total calibration resources, and skills that are needed to achieve the required quality.

-  Address for compatibility the delivery of service, calibration process, installation, servicing, inspection and test procedures, and the applicable documentation.

-  Address the need to develop and implement new techniques, and the criteria for acceptance, as necessary.

-  Address measurement requirements involving capability that exceeds the known state of the art.

-  Clarify standards of acceptability, including those that contain a subjective element.

-  Identify quality records that are necessary to provide confidence of conformity of the processes and results, and the records are maintained as described in documented procedures.

Quality Procedure QP 7100 describes the planning for service delivery and refers to other Mythical True Value Metrology quality procedures involved in the process.

Customer-Related Processes
--------------------------

Determination of Requirements Related to the Equipment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology determines customer requirements including service and/or product requirements specified by the customer. These customer requirements include, but are not limited to:

-  specific calibration requirements;

-  the requirements for availability, and delivery, with associated costs;

-  contract requirements not specified by the customer but necessary for intended or specified use; and

-  obligations related to the contract, including regulatory and legal requirements.

.. note:: Mythical True Value Metrology is established by Mythical Airlines (its principal customer) to provide calibration and related services. Contract review or other customer determination processes are not an on-going requirement with respect to the calibration service that we provide to Mythical Airlines. If equipment is delivered to us using Mythical Airlines’ processes and no exceptions are noted, the customer and we expect that routine calibration service will be provided with no need for further determination or review.

Quality Procedure QP 7200 is established to implement and provide further guidance regarding customer-related processes.

Review of Requirements Related to the Product
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If equipment is delivered to us using Mythical Airlines’ processes and no exceptions are noted, the customer and we expect that routine calibration service will be provided with no need for further determination or review. Should that expectation change due to a change in Mythical Airlines policy, it will be reviewed and documented at management review.

Mythical True Value Metrology ensures that customer requirements are fully understood and the department has the capability to meet them. Before acceptance of a non-routine customer requirement, including work from customers other than Mythical Airlines, the request is reviewed by the department to ensure that:

-  Requirements are adequately defined and documented.

-  Differences between the standard requirements and those in the request are resolved.

-  Mythical True Value Metrology has the capability and capacity to meet these requirements.

If a change to the standard requirements is identified by the laboratory or requested by the customer, the department documents such statements of or changes to requirements. The department ensures that relevant documentation is amended and relevant personnel are made aware of the changed requirements.

Customer Communication
~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies and implements arrangements for communication with customers relating to:

-  calibration information;

-  inquiries, or order handling, including amendments;

-  customer feedback, including customer complaints.

Design and development
----------------------

Mythical True Value Metrology is not currently engaged in the design or development of product. Mythical True Value Metrology does not design or develop the equipment or service used in the calibration process, or the equipment that is calibrated. The equipment calibration is based on verifiable performance specifications and requirements of the equipment manufacturer and/or the customer. Work instructions (calibration procedures) are either publicly available methods or methods written by the metrology engineering staff based on publicly available methods. In either case, the methods are verified and validated according to generally acceptable metrology principles before being accepted for use. Therefore, clause 7.3 of the ISO 9001 standard is not currently applicable. Should the department become involved in product design, then it will define and document suitable controls to ensure conformance to specified requirements.

Purchasing
----------

Purchasing of outside materials, supplies, and services is not a burdensome requirement for Mythical True Value Metrology. In most cases, Mythical Airlines is considered the supplier of all purchasing services.

-  All purchases of capital items, high volume consumables, items or services requiring a purchase order, contracted services, and so on are largely handled by other Mythical Airlines departments and are under the control of Mythical Airlines’ policies and procedures and therefore outside the scope of this QMS.

-  Much of the purchasing process uses Mythical Airlines requirements, controls and staff, which are outside the scope and controls of the Mythical True Value Metrology QMS.

-  Suppliers that may be used with a purchase order or through the electronic business-to-business system are controlled by the Mythical Airlines corporate purchasing and accounting departments, which are outside the scope of this QMS.

-  Some materials and supplies are obtained from the Mythical Airlines stock system.

-  Support processes are supplied and controlled by other Mythical Airlines departments.

Mythical True Value Metrology directly purchases some materials, supplies, and services from other companies by using Mythical Airlines’ corporate purchase card. Almost all such purchases are infrequent or non-recurring retail purchases of very small quantities of cataloged or other commercial off-the-shelf products. All purchases must follow the policies and procedures of Mythical Airlines, which are outside the scope of this QMS.

Mythical True Value Metrology maintains a local list of active outside (non-Mythical Airlines) suppliers and the materials or services they provide, but suppliers are primarily managed at the corporate level and must be listed in the corporate enterprise management system database.

Mythical Airlines provides for the inspection and auditing of suppliers, when a need is determined, according to Mythical Airlines policy. In this respect Mythical Airlines is a supplier of this service to Mythical True Value Metrology.

Mythical True Value Metrology, where practical, verifies purchased material, supplies, and services before use or redistribution.

Purchasing Process
~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology ensures that purchased products and services conform to specification and are purchased from approved sources. Purchasing control is dependent on the type of product, on the effect on subsequent service realization processes and their output, and where applicable, on the quality audit reports and/or quality records of suppliers’ previously demonstrated capability and performance. In all cases, incoming product is not used until it is inspected or otherwise verified against specified requirements.

Mythical True Value Metrology has established and maintains local records of acceptable suppliers. An approved supplier list (ASL) is maintained. A critical supplier list is also maintained.

QP 7400 defines the Mythical True Value Metrology process for purchasing controls over its suppliers. Separate procedures detail the processes for purchasing and materials, supplies and services other than calibration (QP 7410.1), and subcontracted calibration services (QP 7410.2).

Purchasing Information
~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology requires that purchasing documents contain data clearly describing the product ordered including, where appropriate, requirements for approval or qualification of

-  product and/or services,

-  procedures,

-  processes,

-  equipment, and

-  personnel

All appropriate and relevant contract clauses have been supplied to Mythical Airlines purchasing department, and all requests for external services or supplies identify the relevant clauses to be used. All purchasing documents are reviewed and approved to ensure the adequacy of specified requirements contained in the purchasing documents prior to their release.

Verification of Purchased Product
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies and implements the activities necessary for verification of purchased products and/or services to ensure that purchase specifications are met.

Mythical True Value Metrology ensures that incoming material is not used or processed until it has been inspected or otherwise verified to specified requirements. Verification of conformance is in accordance with the quality plan and/or documented procedures.

Mythical True Value Metrology has determined the amount and nature of receiving verification, based on the extent of control exercised at the supplier premises and the recorded evidence of conformance provided.

Verification of purchased product or service by Mythical True Value Metrology at the supplier’s premises is not required at this time. If it ever does become a requirement, this process will be re-evaluated.

Verification of purchased product or service by the Customer is not required at this time. Such verification would required recalibration of the instrument, and the customer does not have the facilities, measurement standards, or qualified personnel to perform the verification. If it ever does become a requirement, this process will be re-evaluated. However, this does not absolve Mythical True Value Metrology of the responsibility to provide acceptable product, nor will it preclude subsequent rejection by the customer.

Calibration and service provision
---------------------------------

Control of Production and Service Provision
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies and plans calibration processes that directly affect quality to ensure that these processes are carried out under controlled conditions. The plan is modified, if necessary, based on review of new customer requirements.

These plans are executed under controlled conditions. Controlled conditions include the following:

-  The availability of information that specifies the product/calibration characteristics.

-  Use of documented work instructions defining the manner of calibration, where the absence of such instructions could adversely affect quality.

-  Use of suitable calibration equipment in a suitable work environment.

-  Compliance with reference standards, codes, control plans, and/or documented procedures and work instructions. Calibration personnel have access to appropriate product specifications information and control measures at each stage of the process.

-  Monitoring and control of suitable product characteristics and process parameters, and the availability and use of measuring and monitoring devices.

-  The approval of process and equipment as required.

-  Criteria for workmanship are stipulated in the clearest practical terms using written standards, representative samples, or illustrations, as appropriate.

-  Suitable maintenance of equipment to ensure continuing process capability.

-  The implementation of defined processes for release, delivery, and any applicable post-delivery activities.

Validation of Processes for Calibration Provision
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Work processes (calibration procedures) are validated before first use and periodically as necessary as determined by Metrology Engineering, proficiency test results, observed deficiencies, or measurement system changes. The nature of a calibration service is that all of the customer's product is either verified to be in good working order before release, or rejected as unusable.

The need for special processes is confirmed or ruled out during product planning. See Quality Procedure QP 7100.

If applicable, Mythical True Value Metrology will validate any calibration and service processes where the resulting output cannot be verified by subsequent measuring or monitoring. This includes any processes where deficiencies may become apparent only after the equipment is in use or the service has been delivered. Currently there are no special processes in use by Mythical True Value Metrology where the results cannot be fully verified by subsequent inspection or testing.

Validation demonstrates the ability of the processes to achieve planned results. Mythical True Value Metrology defines arrangements for validation, including the following, as applicable:

-  qualification of processes;

-  qualification of equipment and personnel;

-  use of defined methodologies and procedures;

-  requirements for records;

-  re-validation.

Identification and Traceability
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology ensures that equipment in a work process is adequately identified by suitable means throughout the process. The department also provides for physical traceability (identification of location and step in the process).

Identification
^^^^^^^^^^^^^^

Equipment is adequately identified through all stages by means of a Mythical Airlines property ID number, or the property ID number provided by an external customer, as applicable.

Traceability
^^^^^^^^^^^^

When and to the extent that physical traceability is a specified requirement, Mythical True Value Metrology will establish and maintain documented procedures for the unique identification of product.

.. note:: Traceability in this sense refers to physically tracking a product through the service provision process and not to metrological traceability of measurement results.

Inspection and Test Status
^^^^^^^^^^^^^^^^^^^^^^^^^^

Mythical True Value Metrology identifies the status of the equipment with respect to measurement and monitoring requirements specified. The identification of inspection and test status is maintained throughout the process to ensure that only equipment that has passed required inspections, tests, and calibration are dispatched or used as serviceable items.

-  Suitable means of identification are in place throughout the calibration process.

-  Computer records are the primary means of identification of status.

-  Job numbers, tags, labels, inspection logs, traveler sheets, and so forth may also be used when appropriate.

-  Since all operations are performed on unique single items, any method relating to a lot of more than one item is by definition not appropriate.

Customer Property
~~~~~~~~~~~~~~~~~

Mythical True Value Metrology exercises care with customer property while it is under our control or being used by the organization. The department identifies, verifies, protects, and maintains customer property provided for service. Any occurrence of customer property that is lost, damaged, or otherwise found to be unsuitable for use is recorded and reported to the customer for disposition.

Customer property includes intellectual property (information provided in confidence) and proprietary information. All personnel are responsible for following Mythical Airlines' general corporate policies on confidentiality. These policies apply equally to intellectual property of external customers.

Preservation of Product
~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology preserves conformity of equipment with customer requirements during internal processing and delivery to the intended destination. This includes identification, handling, packaging, storage, and protection. This also applies to the constituent parts of and accessories for a product.

Quality Procedure QP 7550 provides for the preservation of equipment quality through all stages.

Control of Monitoring and Measuring Devices
-------------------------------------------

.. note::

    This section applies to the calibration and maintenance of Mythical True Value Metrology’s own equipment used to provide its calibration services.

    In this respect, Measuring and Monitoring Devices may also be referred to as measurement standards, calibration standards, or laboratory standards. All of these terms are equivalent.

Mythical True Value Metrology identifies the measurements to be made and selects the measuring and monitoring devices required to assure conformity of calibration to specified requirements.

Adequately calibrated measuring and monitoring devices (measurement standards) are used and controlled to provide assurance that measurement capability is consistent with the measurement requirements, and that measurement results are traceable to the International System of Units.

Where applicable, Mythical True Value Metrology ensures that measuring and monitoring devices:

-  are calibrated on a defined periodic basis and adjusted when the need is indicated, against devices traceable to nationally recognized standards. Where no such standards exist, the basis used for calibration is agreed upon and recorded;

-  are safeguarded from adjustments that would invalidate the calibration;

-  are protected from damage and deterioration during handling, maintenance, and storage;

-  calibration results are recorded;

-  have the validity of previous results re-assessed if measuring and monitoring devices are subsequently found to be out of calibration, and corrective action taken.

Where software is used to control measuring and monitoring of specified requirements, it is validated prior to initial use. Once the software has been verified to function as intended (as part of the receiving inspection process), then no further tests are required unless a version upgrade is installed or a skilled operator has reason to suspect a problem.

Quality Procedure QP 7600 provides additional guidance on control of measurement standards and implements this policy.

Substitution of Test and Measurement Equipment Used as Measurement Standards
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When the measurement standard called for in the calibration procedure does not exist or is not available on a more or less permanent basis, a substitution may be required. The substitute equipment may also be referred to as “equivalent” or “alternate” equipment.

When the calibration procedure used is documented in an aircraft, engine or component maintenance document controlled by the controlled maintenance manual procedures of Mythical Airlines, or an Airworthiness Directive, any substitute for the measurement standard must be requested and approved through Mythical Airlines' corporate Tooling Equivalency process. The work cannot be accomplished until approval is documented and received.

In all other cases, a substitute standard request must be submitted to the metrology engineering staff for approval and documentation.