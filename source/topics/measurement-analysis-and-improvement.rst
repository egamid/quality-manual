
--------------------------------------
Measurement, analysis, and improvement
--------------------------------------


Mythical True Value Metrology defines, plans, and implements the measurement and monitoring activities needed to ensure conformity and achieve improvement. This includes the determination of the need for, and use of, applicable methodologies including statistical techniques.

Monitoring and Measurement 
--------------------------

Customer Satisfaction
~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology monitors information on customer satisfaction and/or dissatisfaction as one of the measurements of performance of the QMS. The quality manager determines the methods for obtaining and using this information.

Internal Audit
~~~~~~~~~~~~~~

Mythical True Value Metrology conducts periodic internal audits, or schedules equivalent audits by other Mythical Airlines departments or by external contractors. The purpose of the internal audit is to determine whether the department's QMS:

-  conforms to the requirements of the Standard;

-  is being effectively implemented and maintained.

Internal quality audits are planned and scheduled on the basis of the status and importance of the activity to be audited. Trained personnel independent of those having direct responsibility for the activity being audited carry out the audits, thus ensuring that auditors do not audit their own work. Since Mythical True Value Metrology is so small that independence cannot always be fully assured, the department may arrange for the internal audit function to be performed by other qualified parties. Examples of other qualified parties include but are not limited to:

-  Agents of the customer, such as Mythical Airlines' own internal quality audit department.

-  Independent third-party organizations, such as a qualified consultant or consulting firm.

-  Qualified internal auditors from other departments of Mythical Airlines.

Mythical True Value Metrology currently schedules internal audits that are performed by Mythical Airlines' own internal quality audit department.

The results of the internal audits are documented and brought to the attention of the personnel having responsibility for the area audited. Management personnel responsible for the area take timely corrective action on the deficiencies found during the audit.

Follow-up activities verify and record the implementation of the corrective action, report the verification results, and close out the audit. Subsequent audits verify the effectiveness of the corrective actions taken.

Results of internal audits and the corrective action are submitted for management review.

QP 8220 implements this policy.

Monitoring and Measurement of Processes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology applies suitable methods for measurement and monitoring of the realization processes necessary to meet customer requirements. These methods confirm the continuing ability of each process to satisfy its intended purpose. Suitable methods are determined by Metrology Engineering and may include process control checks, proficiency tests, and other methods.

Monitoring and Measurement of Product
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The purpose of Mythical True Value Metrology is to measure and monitor the characteristics of the equipment being calibrated to verify that requirements are met. This is carried out at appropriate stages by the calibration process and may include equipment testing and/or inspection as required by the quality plan and/or documented work instructions.

Evidence of conformity with the acceptance criteria is documented. Records indicate the authority responsible for release of product.

Mythical True Value Metrology carries out final inspection in accordance with the quality plan and/or documented procedures to complete the evidence of conformance of the finished product/service to specified requirements. See QP 4106.

The quality plan and/or documented procedures require that all specified inspections and tests and calibrations, including those specified on receipt or in process, have been carried out and that the results meet specified requirements.

Equipment release and service delivery does not proceed until all the specified activities have been completed and the associated data and documentation are available and authorized unless otherwise approved by the customer.

Mythical True Value Metrology has established and maintains records that provide evidence that equipment has been inspected and/or tested and calibrated. These records show clearly whether the equipment has passed or failed according to defined acceptance criteria. Where the equipment fails to pass any inspection, the process defined in the applicable Mythical Airlines policy (outside the scope of this Business Management System) is followed. In the case of equipment owned by an external customer, the customer is notified and the item is returned as rejected with the specific deficiencies noted.

Records identify the calibration authority responsible for the release of the equipment.

Control of Nonconforming Product
--------------------------------

.. note:: As noted in sections 2.2 and 7 of this Business Operating Manual, the "product" of this organization is the service of performing calibration of precision electronic test and measuring instruments.

Mythical True Value Metrology ensures that service provided which does not meet requirements is identified, controlled where possible to prevent unintended use or delivery to the customer, and corrected if it has been delivered. This policy is implemented in QP 8300. This procedure includes provisions for:

-  identification, documentation, evaluation, segregation (where practical), disposition of nonconforming service, and for notification of the functions concerned;

-  assigning responsibility for the review and the authority for disposition of nonconforming service;

-  correction of nonconforming service and re-verification/calibration of the affected equipment after correction to demonstrate conformity (if necessary);

-  handling of nonconforming service when it is detected after delivery to the customer.

Analysis of Data 
----------------

Mythical True Value Metrology collects and analyzes appropriate data to determine the suitability and effectiveness of the QMS and to identify improvements that can be made. This includes data generated by measuring and monitoring activities and other relevant sources.

The data are analyzed to provide information on:

-  customer satisfaction and/or dissatisfaction;

-  conformance to customer requirements;

-  characteristics of processes, product and their trends;

-  performance of suppliers.

Improvement
-----------

Continual Improvement
~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology plans and manages the processes necessary for the continual improvement of the QMS.

Mythical True Value Metrology facilitates the continual improvement of the QMS through the use of the quality policy, objectives, audit results, analysis of data, corrective and preventive action and management review.

Corrective Action
~~~~~~~~~~~~~~~~~

Mythical True Value Metrology takes corrective action to eliminate the cause of identified nonconformities in order to prevent recurrence. Corrective actions are appropriate to the impact of the problems encountered.

QP 8520 defines requirements for:

-  identifying nonconformities (including customer complaints);

-  determining the causes of nonconformity;

-  evaluating of the need for actions to ensure that nonconformities do not recur;

-  determining and implementing the corrective actions needed;

-  recording results of action taken;

-  review and evaluation of corrective action taken to assess its effectiveness.

Preventive Action
~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies preventive actions to eliminate the causes of identified potential nonconformities to prevent initial occurrence. Appropriate sources of information such as processes and work operations results which affect product quality, concessions, audit results, quality records, service reports, and customer complaints are analyzed (see 8.4 above) to detect preventive action possibilities. Preventive actions taken are appropriate to the impact of the potential problems.

QP 8530 defines requirements for:

-  identification of potential nonconformities and their causes;

-  determination of the steps needed to eliminate identified causes and completion of the preventive action implementation;

-  recording results of action taken;

-  review and evaluation of preventive action taken to assess its effectiveness;

-  ensuring that relevant information on actions taken, including changes to procedures, is subject to management review.