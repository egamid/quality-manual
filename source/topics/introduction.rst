
------------
Introduction
------------

This Quality Manual demonstrates and documents Mythical True Value Metrology’s commitment to maintaining a high-level of quality and strong customer service within an environment that has safety as a first priority, is focused on the customers, and fosters continual improvement.

Since the philosophy of Mythical True Value Metrology is that Quality is an integral part of the entire business operation, hereafter this Quality Manual will be referred to as the **Business Operating Manual**. The organization is in the process of implementing that change. As part of the implementation, the term “Quality Management System” is also being changed to **Business Management System**.

Conformance and Compliance Standards
------------------------------------

The Business Operating Manual is intended to demonstrate conformance to

   ANSI/ISO/ASQ Q9001-2008 *American National Standard: Quality management systems — Requirements*. This standard is the United States’ legal equivalent of the ISO 9001:2008 international standard. These two reference numbers may be used interchangeably in this manual and the quality management system. In all other references to this conformance standard in this manual or quality management system documents, the reference to the year of the current edition is not used. Reference to this conformance standard also implies reference to all guidance standards contained therein.

In addition, because Mythical Airlines is a scheduled air carrier that performs its own maintenance and is an FAA licensed Repair Station for maintenance work performed for other airlines, this Business Operating Manual is intended to demonstrate compliance with relevant regulatory issues from

   Federal Aviation Regulations Part 121 (14 CFR 121) and Part 145 (14 CFR 145).

Other Relevant Standards
------------------------

In addition to the conformance and compliance standards, Mythical True Value Metrology may from time to time use other standards documents as guidance in its operations and Quality Management System. These may include but are not limited to current editions of the following:

-  ANSI/NCSL Z540.2 *American National Standard — Guide to the expression of uncertainty in measurement*

-  ANSI/NCSL Z540.3 *American National Standard — Requirements for the calibration of measuring and testing equipment*

-  ISO 10012 *Measurement management systems — Requirements for measurement processes and measuring equipment*

-  ISO/IEC 17025 *General requirements for the competence of testing and calibration laboratories*

With the exception of ISO/IEC 17025, these are not auditable standards and are used solely for guidance. ISO/IEC 17025 is currently used only for guidance as it is part of the laboratory's continual improvement process to become accredited to that standard at a future date.

Glossary
--------

.. glossary::
    
    ANSI
     American National Standards Institute
     
    ASQ
     American Society for Quality
     
    FAA
     Federal Aviation Administration (the USA regulatory agency for aviation and aerospace operations.)
     
    IEC
     International Electrotechnical Commission
     
    ISO
     International Organization for Standardization
     
    Metrology
     The science and practice of precision measurement, specifically the various disciplines of calibration required by Mythical True Value Metrology customers
     
    NCS
     National Conference of Standards Laboratories, now known as NCSL International (NCSLI)
     
    PDF
     Portable Document Format, a file system extension used to designate a document that conforms to the requirements of international standard ISO 32000-1, *Document management – Portable document format – Part 1: PDF 1.7*
     
    QMS
     Quality Management System. Mythical True Value Metrology is in the process of changing this to BMS – Business Management System. The change will be fully implemented in this document after all level 2 documents have been updated.
     
    RSA
     An algorithm for public-key cryptography which is suitable for digital signatures. This type of digital signature both authenticates the signer as the only person who could have signed it, and authenticates that the document has not been changed since it was signed. (RSA stands for Rivest, Shamir and Adleman, authors of the Massachusetts Institute of Technology paper that disclosed the method in 1978.)

Services Offered
----------------

Mythical True Value Metrology is a department of Mythical Airlines. The product of Mythical True Value Metrology is the service of providing calibration of tools, precision measuring equipment and testing equipment. The department performs, or arranges for, the calibration of all such equipment owned by or used by Mythical Airlines in all operating stations, as well as that of other aviation customers as arranged by contract. The department was created in 1995 and has been registered to ISO 9001 since August 2001. The department’s vision is to be the leading metrology facility in the airline industry and provide strong support toward continually building a superior Mythical Airlines team.

Parent Organization 
-------------------

Mythical Airlines, Incorporated, is an international airline which carries more passengers annually than any other airline in the world. Based in Smallville, Kansas, USA, Mythical Airlines has had from its earliest days a strong Midwestern reputation for legendary safety, service, speed, and strength around the world. The company’s vision is to create the world’s most trusted airline by becoming “Number One” in the eyes of our customers. The corporate strategies to accomplish this are:

-  Operate an airline network that takes passengers from anywhere to anywhere with unmatched safety, quality of service, and speed;

-  Excel in building a superior Mythical Airlines team;

-  Leverage Mythical Airlines’ strengths for the good of our customers, employees and other stakeholders;

-  Achieve superior financial results while acting ethically and dealing fairly, consistent with generally accepted superior values, and in conformance to all laws and regulations.

Purpose of This Manual
----------------------

Mythical True Value Metrology’s overall commitment to quality in work practice and customer service is defined through its Core and Support Business Processes. Through each of these business processes, the Business Management System is aligned with the goals and strategic direction of the organization. The Business Management System as described in this Business Operating Manual defines the department’s commitment:

-  by demonstrating its ability to consistently provide quality service that meets customer and applicable regulatory requirements,

-  by addressing customer satisfaction through the effective application of the system, including processes for continual improvement and the prevention of nonconformity,

-  through employee empowerment, especially for innovative action to improve the department’s performance, and

-  through orderly change management that will maintain a high level of service in technologically complex and fast-paced environments, both to accommodate technological change and for continual improvement of the technical skill and capability.

This Business Operating Manual provides an overview of the quality policies and key requirements for the department. It is the source of reference for all matters dealing with quality. It is available for inspection by our customers, potential customers, third party quality auditors, and regulatory agencies.

Scope of the Business Management System and Exclusions
------------------------------------------------------

**Scope:** Calibration service for tools, precision measuring equipment and testing equipment for Mythical Airlines and other customers. The scope of the QMS is specifically defined as everything within the direct control and authority of Mythical True Value Metrology **only**. Many functions are performed by other departments of Mythical Airlines; these departments are treated as customers and/or suppliers, as appropriate.

**Exclusions:** ISO 9001 clause 7.3 Design and Development.

**Mitigated:**  ISO 9001 clause 7.4 Purchasing.

Level 1 Business Operating Manual 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This Level 1 Business Operating Manual and its subsidiary documents describe the Business Management System of Mythical True Value Metrology, and complies with all applicable requirements of the ISO 9001 International Standard. It addresses pertinent requirements of the standard and includes references to documented quality procedures that apply to Mythical True Value Metrology. Compliance is demonstrated through the formal ISO registration process.

.. important:: The system described in this manual is **in addition to** the quality and technical policies and procedures defined by Mythical Airlines or mandated by FAA and other regulatory agencies. These other requirements are followed and may be referenced in some documents for clarity. However, they are outside the scope and control of this Department's own QMS. Nothing in this manual should be interpreted to supersede or contradict these external requirements.

Subsidiary Level 2 Documents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In addition, there are several subsidiary (Level 2) quality documents that include information on:

-  Organizational structure

-  Quality responsibilities and authorities, including Management Representative appointment

-  Mythical True Value Metrology’s business processes including core and support processes

-  Core Business Processes address the department’s calibration process — they define the sequence of processes and sub-processes required to achieve contracted requirements and address applicable requirements of clause 7 of ISO 9001, and regulatory requirements.

-  Support Business Processes address infrastructure requirements such as those described by clauses 5, 6 and 8 of ISO 9001, and regulatory requirements.

Level 2 documents are referenced in this Business Operating Manual but are not part of it.

Laboratory Network
~~~~~~~~~~~~~~~~~~

The Laboratory computer network is an important piece of the Mythical True Value Metrology QMS. This network contains the documents and applications that define the management controls by which the department demonstrates conformance to its QMS. It also demonstrates the effectiveness of the QMS towards attainment of the department’s management objectives.

This network contains at least the following documents and records:

-  The Mythical True Value Metrology Business Operating Manual, Quality Procedures, and other instructions that implement the policies in the Business Operating Manual.

-  Internal Auditing Information: including audit schedule, audit checklists that detail the department management controls, plus conformance criteria; conformance responsibility; and objective evidence criteria for each control. These controls are further described in the corresponding level II quality procedures.

-  Audit findings that assess compliance and effectiveness and needs for improvement

-  Corrective and preventive activities for continual improvement

-  Summaries of Management Review meetings, including action items and their current status of accomplishments.

-  Other support documents as needed to supplement this information.

Permissible Exclusions and Mitigation 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology does not design or develop the equipment or service used in the calibration process, or the equipment that is calibrated. The equipment calibration is based on published and verifiable performance specifications and requirements of the equipment manufacturer and/or the customer. Therefore, clause 7.3 of the ISO 9001 standard is not applicable.

Mythical True Value Metrology does not directly procure equipment, services or most supplies. A procurement action must be initiated by the department and then be processed and handled by other departments of Mythical Airlines. The only exception is permissible small purchases using a corporate purchase card. Therefore the requirements of clause 7.4, Purchasing, are mitigated to the extent that they are outside the scope of Mythical True Value Metrology's authority and Business Management System.