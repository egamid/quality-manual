
-----------------------
Distribution and review
-----------------------

Circulation List
----------------

The Business Operating Manual is maintained on the Mythical True Value Metrology Laboratory network as a read-only document and is available to all Department employees and to other interested and authorized parties. All copies printed from the network are reference copies and are uncontrolled. Electronic copies other than the one visible on the laboratory internal network are reference copies and are uncontrolled.

Procedures for Updating the Business Operating Manual
-----------------------------------------------------

The Business Operating Manual will be reviewed by Mythical True Value Metrology management and revised as required. After management review, the Business Operating Manual is updated (if needed) by incorporating all approved changes. The Management Representative may approve minor changes and updates to the Business Management System. (Minor changes include, for example, spelling corrections, punctuation corrections where the sense of the sentence is not changed, or maintaining hyperlinks and references to other documents on the network.) The revision date is updated, and the document is re-approved and released as a new issue in its entirety. The revision data on the cover refers to the entire document. Procedures for quality policies and procedures documents within the QMS are covered in :ref:`Control of Documents`, and implemented by QP 4230.