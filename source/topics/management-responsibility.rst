
-------------------------
Management responsibility
-------------------------

Management Commitment
---------------------

The top management of Mythical True Value Metrology is committed to the development and improvement of an effective Quality Management System. This commitment is demonstrated by:

-  Communicating to the organization the importance of meeting all customer, regulatory, and legal requirements. (:ref:`Internal Communications`)

-  Establishing the Mythical True Value Metrology Quality Policy for total commitment to excellence and the associated quality objectives. The Quality Policy and the Quality Objectives are published and displayed on the Laboratory network sign-in page and the office notice board. (Sections 5.3 Quality Policy and 5.4.1 Quality Objectives.)

-  Conducting Management Reviews. (:ref:`Management Review`)

-  Identifying and acquisition of controls, processes, equipment, fixtures, resources, and skills need to achieve the required quality. The associated quality management activities are described in :ref:`Resource Management`.

Customer Focus
--------------

Top management of Mythical True Value Metrology ensures that customer needs and expectations are determined, converted into requirements, and fulfilled with the aim of meeting or exceeding those expectations. The general needs of our customers are defined by the fact that Mythical True Value Metrology was established by the parent company to perform the specific service of metrology. The specific needs of our customers are determined through continual verbal and written communications, as well as periodic visits to their facilities to better understand their individual processes. It is the responsibility of all personnel that interface with customers to access stated or implied needs and bring them to the attention of top management so that they may be addressed. To continually improve, the department also monitors, measures, and analyzes customer satisfaction throughout the business cycle.

The department also meets all of its other obligations including regulatory and legal requirements. This is further described in :ref:`Determination of Requirements Related to the Equipment`.

Quality Policy
--------------

The top management of Mythical True Value Metrology has defined and documented its Quality Policy. This policy includes the organization’s commitment for meeting customer requirements and to continual improvement.

**Quality Policy Statement:**

-  The Quality Policy of Mythical True Value Metrology is to provide high and consistent quality in the calibration and servicing of precision tools, inspection, and measuring and test equipment owned by Mythical Airlines and other customers. We are always mindful of the fact that the items we calibrate and repair are used in maintenance of aircraft and all of their systems, an environment where safety is paramount.

-  Our commitment is to the safety and accurate work of our customer's professionals who use the items we have calibrated, thereby ensuring the safety and comfort of aviation passengers and other customers.

-  All work is done in conformance to Mythical True Value Metrology’s QMS, the applicable technical and administrative operating policies and procedures of Mythical Airlines, legal and regulatory requirements, and specific customer requirements.

-  Through front-line input and management leadership, we will continue to improve our people and processes to anticipate, meet, and exceed the needs of our customers.

-  We support the continually improving quality of our customer's maintenance and other technical operations through the the services we provide.

The quality policy and quality objectives are relevant to the department’s organizational goals and the expectations and needs of its customers. These concepts have been communicated to all employees within the scope of the QMS. Quality concepts have been integrated into Mythical True Value Metrology’s culture.

The top management of Mythical True Value Metrology ensures that its Quality Policy is understood. This is accomplished through training, communication, and displays. The Policy is implemented by means of documentation, and training about the Business Management System to meet the requirements of the Standard. The Policy is maintained through Business Management Systems audits and corrective and preventive action, and reviewed for continuing suitability through periodic Management Reviews. Further, this Quality Policy is supported by the quality objectives listed in this Business Operating Manual. (:ref:`Quality Objectives`).

Planning
--------

Quality Objectives 
~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology establishes quality objectives at all relevant functions and levels within the organization. The objectives are derived from Mythical True Value Metrology’s Quality Policy and Mythical Airlines corporate policies. The quality objectives are documented (Annual Quality Objectives) and are reviewed by top management.

The quality objectives are measurable and consistent with the quality policy, including commitment to continual improvement. Quality objectives include those needed to meet all contracted requirements. Inherent in the entire set of quality objectives is a focus on customer satisfaction.

Quality Management System Planning 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology has identified and planned the resources needed to achieve its quality objectives. The results are documented in management reviews. Quality planning addresses continual improvement of the QMS and assures that change is conducted in a controlled manner so that the integrity of the QMS is maintained during this change.

The Quality Plan identifies the core and support business processes of the QMS, and is documented in the form of references to associated Quality Procedures. The Core Business Processes represent how we turn opportunity into profitable outcomes. Its effectiveness is maintained and enhanced by Support Processes. The identified Core and Support business processes are indicated in :numref:`Fig. {number} <core-support-business-processes>`

.. figure:: figures/core-support-business-processes.png
    :width: 500px
    :name: core-support-business-processes
    
    Core support business processes
    
Each core and support business process has a process owner who has the prime responsibility for ensuring that the process:

-  achieves its objectives, and

-  is under continual review for improvement.

The business processes, their associated procedures, and the business process owners are shown in the Business Process and Responsibility Matrix. Each of the core business processes and support processes is linked to a specific Level 2 quality procedure.

The Quality Plan describes the overall plan for quality assurance. The quality plan consists of

-  the core business processes and their interactions,

-  the support processes and their interactions,

-  the Business Process and Responsibility matrix, and

-  appropriate outputs of management review, such as supporting resources.

The Quality Plan is assessed for on-going suitability and effectiveness during management review meetings.

Responsibility, Authority and Communication
-------------------------------------------

Responsibility and Authority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology has defined business functions and their interrelations within the organization, including responsibilities and authorities; and those are communicated in order to facilitate effective quality management. For the purpose of this Business Operating Manual and the quality management system, **top management of Mythical True Value Metrology is the Vice-President — Quality of Mythical Airlines**.

Mythical True Value Metrology ensures the responsibility, authority and interrelationship of personnel who manage, perform, and verify work affecting quality is defined and documented, particularly for personnel who need the organizational freedom and authority to:

-  Initiate action to prevent the occurrence of any nonconformance relating to service, process, and Business Management Systems,

-  Identify and record any problems relating to the service, process, and Business Management System,

-  Initiate, recommend, or provide solutions through designated channels,

-  Verify the implementation of solutions,

-  Control further processing, delivery or installation of nonconforming product until the deficiency or unsatisfactory condition has been corrected.

The Mythical True Value Metrology organization chart :numref:`Fig. {number} <organization-and-top-management>` shows the organization of the department and the relationships to the Mythical Airlines corporate management. Relationships to other parts of the Mythical Airlines corporate structure are documented by Mythical Airlines and are available on the Mythical Airlines network. In the organization chart, the portion INCLUDED in this quality management system is shaded in blue. The rest of the company is treated as customers or suppliers, as appropriate.

Management Representative
~~~~~~~~~~~~~~~~~~~~~~~~~

The top management of Mythical True Value Metrology has appointed the Mythical Airlines Director – Metrology as its Management Representative. This individual, irrespective of other responsibilities, has the defined authority to:

-  Ensure that the business processes of the QMS are defined.

-  Ensure that the Business Management System requirements are established, implemented and maintained in accordance with the Standard.

-  Report to top management on the performance of the QMS, including needs for improvement.

-  Promote awareness of customer requirements throughout the organization.

The Management Representative also acts as the liaison between the department and other third parties on matters concerning the department Business Management System.

The Metrology Engineering and Support Manager is the Deputy Management Representative.

.. figure:: figures/organization-and-top-management.png
    :width: 500px
    :name: organization-and-top-management
    
    Organization and top management
    
Internal Communications 
~~~~~~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology ensures that communication is maintained between its various levels and functions regarding the processes of the QMS and their effectiveness. This is accomplished via the Laboratory network, various quality meetings, and other internal publications.

Management Review
-----------------

Top management of Mythical True Value Metrology reviews the QMS at planned intervals to ensure its continuing suitability, adequacy and effectiveness. The review evaluates the need for changes to the organization's QMS, including its quality policy and quality objectives.

Review Input
~~~~~~~~~~~~

Inputs to management review include but are not limited to current performance and improvement opportunities related to the following:

-  results of audits;

-  customer feedback;

-  process performance and product conformance;

-  status of preventive and corrective actions;

-  follow-up actions from earlier management reviews;

-  changes that could effect the QMS; and

-  recommendations for improvement of the system.

Review Output
~~~~~~~~~~~~~

The outputs from the management review include but are not limited to decisions and actions related to:

-  improvement of effectiveness of the QMS and its processes;

-  improvement of product related to customer requirements; and

-  resource needs.

Results of management reviews are recorded.

