**************
Quality Manual
**************

**Mythical True Value Metrology**

*a department of*

**Mythical Airlines**

**Legendary Safety, Service, Speed and Strength Around the World from**

**Smallville, Kansas, USA**

**BUSINESS OPERATING MANUAL**

(QUALITY MANUAL)

**Revision Date:** 2019-09-23

**Approved By:** Director, Mythical True Value Metrology Date: 2019-09-23

*This document is authenticated in whole by verification of the above digital signature, using the signer's public key.*

   This is a complete re-issue of the manual with all previous changes incorporated, therefore there are no revision marks. This version replaces all prior issues.

   This manual is the property of Mythical True Value Metrology. It must not be reproduced in whole or in part or otherwise disclosed without prior written consent from Mythical True Value Metrology.

   The official controlled copy of this manual is the digitally signed PDF document on the Mythical True Value Metrology network server and visible to all authorized users. All printed copies, and all electronic copies and versions except the ones described above, are considered uncontrolled copies used for reference only.

   This document is controlled as a single entity, as any change – however slight, even a single character – to any part of the document by definition changes the entire document. For this reason, as well as the fact that the concept of “page” varies with the publication format, page-level revision is not practiced with this or any other Mythical True Value Metrology document.

INTRODUCTION
===============

This Quality Manual demonstrates and documents Mythical True Value Metrology’s commitment to maintaining a high-level of quality and strong customer service within an environment that has safety as a first priority, is focused on the customers, and fosters continual improvement.

Since the philosophy of Mythical True Value Metrology is that Quality is an integral part of the entire business operation, hereafter this Quality Manual will be referred to as the **Business Operating Manual**. The organization is in the process of implementing that change. As part of the implementation, the term “Quality Management System” is also being changed to **Business Management System**.

Conformance and Compliance Standards
------------------------------------

The Business Operating Manual is intended to demonstrate conformance to

   ANSI/ISO/ASQ Q9001-2008 *American National Standard: Quality management systems — Requirements*. This standard is the United States’ legal equivalent of the ISO 9001:2008 international standard. These two reference numbers may be used interchangeably in this manual and the quality management system. In all other references to this conformance standard in this manual or quality management system documents, the reference to the year of the current edition is not used. Reference to this conformance standard also implies reference to all guidance standards contained therein.

In addition, because Mythical Airlines is a scheduled air carrier that performs its own maintenance and is an FAA licensed Repair Station for maintenance work performed for other airlines, this Business Operating Manual is intended to demonstrate compliance with relevant regulatory issues from

   Federal Aviation Regulations Part 121 (14 CFR 121) and Part 145 (14 CFR 145).

Other Relevant Standards
------------------------

In addition to the conformance and compliance standards, Mythical True Value Metrology may from time to time use other standards documents as guidance in its operations and Quality Management System. These may include but are not limited to current editions of the following:

-  ANSI/NCSL Z540.2 *American National Standard — Guide to the expression of uncertainty in measurement*

-  ANSI/NCSL Z540.3 *American National Standard — Requirements for the calibration of measuring and testing equipment*

-  ISO 10012 *Measurement management systems — Requirements for measurement processes and measuring equipment*

-  ISO/IEC 17025 *General requirements for the competence of testing and calibration laboratories*

With the exception of ISO/IEC 17025, these are not auditable standards and are used solely for guidance. ISO/IEC 17025 is currently used only for guidance as it is part of the laboratory's continual improvement process to become accredited to that standard at a future date.

Glossary
--------

.. glossary::
    
    ANSI
     American National Standards Institute
     
    ASQ
     American Society for Quality
     
    FAA
     Federal Aviation Administration (the USA regulatory agency for aviation and aerospace operations.)
     
    IEC
     International Electrotechnical Commission
     
    ISO
     International Organization for Standardization
     
    Metrology
     The science and practice of precision measurement, specifically the various disciplines of calibration required by Mythical True Value Metrology customers
     
    NCS
     National Conference of Standards Laboratories, now known as NCSL International (NCSLI)
     
    PDF
     Portable Document Format, a file system extension used to designate a document that conforms to the requirements of international standard ISO 32000-1, *Document management – Portable document format – Part 1: PDF 1.7*
     
    QMS
     Quality Management System. Mythical True Value Metrology is in the process of changing this to BMS – Business Management System. The change will be fully implemented in this document after all level 2 documents have been updated.
     
    RSA
     An algorithm for public-key cryptography which is suitable for digital signatures. This type of digital signature both authenticates the signer as the only person who could have signed it, and authenticates that the document has not been changed since it was signed. (RSA stands for Rivest, Shamir and Adleman, authors of the Massachusetts Institute of Technology paper that disclosed the method in 1978.)

Services Offered
----------------

Mythical True Value Metrology is a department of Mythical Airlines. The product of Mythical True Value Metrology is the service of providing calibration of tools, precision measuring equipment and testing equipment. The department performs, or arranges for, the calibration of all such equipment owned by or used by Mythical Airlines in all operating stations, as well as that of other aviation customers as arranged by contract. The department was created in 1995 and has been registered to ISO 9001 since August 2001. The department’s vision is to be the leading metrology facility in the airline industry and provide strong support toward continually building a superior Mythical Airlines team.

Parent Organization 
-------------------

Mythical Airlines, Incorporated, is an international airline which carries more passengers annually than any other airline in the world. Based in Smallville, Kansas, USA, Mythical Airlines has had from its earliest days a strong Midwestern reputation for legendary safety, service, speed, and strength around the world. The company’s vision is to create the world’s most trusted airline by becoming “Number One” in the eyes of our customers. The corporate strategies to accomplish this are:

-  Operate an airline network that takes passengers from anywhere to anywhere with unmatched safety, quality of service, and speed;

-  Excel in building a superior Mythical Airlines team;

-  Leverage Mythical Airlines’ strengths for the good of our customers, employees and other stakeholders;

-  Achieve superior financial results while acting ethically and dealing fairly, consistent with generally accepted superior values, and in conformance to all laws and regulations.

Purpose of This Manual
----------------------

Mythical True Value Metrology’s overall commitment to quality in work practice and customer service is defined through its Core and Support Business Processes. Through each of these business processes, the Business Management System is aligned with the goals and strategic direction of the organization. The Business Management System as described in this Business Operating Manual defines the department’s commitment:

-  by demonstrating its ability to consistently provide quality service that meets customer and applicable regulatory requirements,

-  by addressing customer satisfaction through the effective application of the system, including processes for continual improvement and the prevention of nonconformity,

-  through employee empowerment, especially for innovative action to improve the department’s performance, and

-  through orderly change management that will maintain a high level of service in technologically complex and fast-paced environments, both to accommodate technological change and for continual improvement of the technical skill and capability.

This Business Operating Manual provides an overview of the quality policies and key requirements for the department. It is the source of reference for all matters dealing with quality. It is available for inspection by our customers, potential customers, third party quality auditors, and regulatory agencies.

Scope of the Business Management System and Exclusions
------------------------------------------------------

=========== ==================================================================================================================================================================================================================================================================================================
Scope:      Calibration service for tools, precision measuring equipment and testing equipment for Mythical Airlines and other customers.
           
            The scope of the QMS is specifically defined as everything within the direct control and authority of Mythical True Value Metrology **only**. Many functions are performed by other departments of Mythical Airlines; these departments are treated as customers and/or suppliers, as appropriate.
Exclusions: ISO 9001 clause 7.3 Design and Development.
Mitigated:  ISO 9001 clause 7.4 Purchasing.
=========== ==================================================================================================================================================================================================================================================================================================

Level 1 Business Operating Manual 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This Level 1 Business Operating Manual and its subsidiary documents describe the Business Management System of Mythical True Value Metrology, and complies with all applicable requirements of the ISO 9001 International Standard. It addresses pertinent requirements of the standard and includes references to documented quality procedures that apply to Mythical True Value Metrology. Compliance is demonstrated through the formal ISO registration process.

.. important:: The system described in this manual is **in addition to** the quality and technical policies and procedures defined by Mythical Airlines or mandated by FAA and other regulatory agencies. These other requirements are followed and may be referenced in some documents for clarity. However, they are outside the scope and control of this Department's own QMS. Nothing in this manual should be interpreted to supersede or contradict these external requirements.

Subsidiary Level 2 Documents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In addition, there are several subsidiary (Level 2) quality documents that include information on:

-  Organizational structure

-  Quality responsibilities and authorities, including Management Representative appointment

-  Mythical True Value Metrology’s business processes including core and support processes

-  Core Business Processes address the department’s calibration process — they define the sequence of processes and sub-processes required to achieve contracted requirements and address applicable requirements of clause 7 of ISO 9001, and regulatory requirements.

-  Support Business Processes address infrastructure requirements such as those described by clauses 5, 6 and 8 of ISO 9001, and regulatory requirements.

Level 2 documents are referenced in this Business Operating Manual but are not part of it.

Laboratory Network
~~~~~~~~~~~~~~~~~~

The Laboratory computer network is an important piece of the Mythical True Value Metrology QMS. This network contains the documents and applications that define the management controls by which the department demonstrates conformance to its QMS. It also demonstrates the effectiveness of the QMS towards attainment of the department’s management objectives.

This network contains at least the following documents and records:

-  The Mythical True Value Metrology Business Operating Manual, Quality Procedures, and other instructions that implement the policies in the Business Operating Manual.

-  Internal Auditing Information: including audit schedule, audit checklists that detail the department management controls, plus conformance criteria; conformance responsibility; and objective evidence criteria for each control. These controls are further described in the corresponding level II quality procedures.

-  Audit findings that assess compliance and effectiveness and needs for improvement

-  Corrective and preventive activities for continual improvement

-  Summaries of Management Review meetings, including action items and their current status of accomplishments.

-  Other support documents as needed to supplement this information.

Permissible Exclusions and Mitigation 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology does not design or develop the equipment or service used in the calibration process, or the equipment that is calibrated. The equipment calibration is based on published and verifiable performance specifications and requirements of the equipment manufacturer and/or the customer. Therefore, clause 7.3 of the ISO 9001 standard is not applicable.

Mythical True Value Metrology does not directly procure equipment, services or most supplies. A procurement action must be initiated by the department and then be processed and handled by other departments of Mythical Airlines. The only exception is permissible small purchases using a corporate purchase card. Therefore the requirements of clause 7.4, Purchasing, are mitigated to the extent that they are outside the scope of Mythical True Value Metrology's authority and Business Management System.

DISTRIBUTION AND REVIEW
=======================

Circulation List
----------------

The Business Operating Manual is maintained on the Mythical True Value Metrology Laboratory network as a read-only document and is available to all Department employees and to other interested and authorized parties. All copies printed from the network are reference copies and are uncontrolled. Electronic copies other than the one visible on the laboratory internal network are reference copies and are uncontrolled.

Procedures for Updating the Business Operating Manual
-----------------------------------------------------

The Business Operating Manual will be reviewed by Mythical True Value Metrology management and revised as required. After management review, the Business Operating Manual is updated (if needed) by incorporating all approved changes. The Management Representative may approve minor changes and updates to the Business Management System. (Minor changes include, for example, spelling corrections, punctuation corrections where the sense of the sentence is not changed, or maintaining hyperlinks and references to other documents on the network.) The revision date is updated, and the document is re-approved and released as a new issue in its entirety. The revision data on the cover refers to the entire document. Procedures for quality policies and procedures documents within the QMS are covered in :ref:`control of documents`, and implemented by QP 4230.

QUALITY MANAGEMENT SYSTEM
=========================

General Requirements
--------------------

Mythical True Value (MTV) Metrology has established, documented and implements an effective QMS as a means of ensuring that its calibration services conform to specified requirements, to foster an environment of continual improvement with an eventual goal of achieving laboratory accreditation to ISO/IEC 17025, and to identify its position as a leading metrology laboratory in the aviation maintenance and repair organization community.

The department’s QMS is comprised of the Core and Support Business Processes that take market opportunities and converts them into value added outputs to satisfy our customers’ needs and expectations.

**Core Processes:**

The core processes describe all the processes that are necessary for the department to realize and deliver the desired service to its customers. QP 4100 is an overview of the processes and their interactions. The Core Processes for the department are listed below:

   Receiving Process (QP 4101)

   Evaluation Process (QP 4102)

   Calibration Process (QP 4103)

   Repair Process (QP 4104)

   Process for control of Outside Services (QP 4105)

   Out-Processing Process (QP 4106)

**Support Processes:**

Mythical True Value Metrology Support Processes describe all other business requirements that are necessary to manage and control resources, and to conduct business in an orderly manner. The support processes are implemented and managed in accordance with the applicable requirements of the International Standard. They include:

   Quality Management (Business Operating Manual)

   Document Management (QP 4230, QP 4240)

   Facility and Equipment Management (QP 6300, QP 6400)

   Information Technology Management (QP 6301)

   Measurement Standards (QP 7600)

   Human Resources (QP 6220)

The sequence and interaction of the Core and Support Business Processes are described in QMS Planning and QP 4100.

The criteria and methods required to ensure the effective operation and control of these processes are defined and documented.

The entire QMS documentation and other key information necessary to support the operation and monitoring of the department business processes are available to all department employees, and other parties requiring access, on the Laboratory network.

Appropriate levels and types of monitoring and measurement of core and supplemental processes have been determined and are documented in relevant policies and procedures.

The QMS includes policies and procedures for implementation of actions required to achieved planned results, and for continual improvement of the business processes.

Documentation Requirements
--------------------------

Mythical True Value Metrology has defined and documented quality procedures consistent with the requirements of the standard. The quality procedures further describe criteria, methods, detail activities, responsibilities and the quality assurance measures that are required to ensure the effective operation and control of the department business processes. The department QMS also includes other documents and records required by the organization to ensure the effective operation and control of the business processes. The degree of documentation is consistent with:

-  the methods used,

-  the complexity and interaction of the business processes, and

-  skills needed and training required by the personnel involved in carrying out these activities.

An outline of the department QMS documentation is shown in :numref:`Fig. {number} <documentation>`

.. figure:: figures/documentation.png
    :width: 850px
    :name: documentation
    
    Documentation
    
The QMS is maintained, updated, and continually improved as the department seeks better business practices. Mythical True Value Metrology employees have been trained on:

-  which procedures apply to them,

-  how to access those procedures,

-  how to apply them to their job function, and

-  how to report related results.

Business Operating Manual
~~~~~~~~~~~~~~~~~~~~~~~~~

A Business Operating Manual (this document, also known as the quality manual) has been established and maintained and includes:

-  the scope of the QMS, including details of and justification for any exclusions;

-  documented procedures or reference to them; and
    
-  a description of the sequence and interaction of the processes included in the QMS.

The Business Operating Manual is a controlled document subject to the requirements of :ref:`Control of Documents`

Control of Documents 
~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology ensures that pertinent issues of documents and data that relate to the requirements of the QMS are controlled. This control also extends to documents of external origin that are maintained solely by Mythical True Value Metrology and not by other departments of Mythical Airlines, such as customer drawings or requirements, performance standards, and service specifications. This policy is implemented by QP 4230.

Document and Data Approval and Issue: 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All documents and data that relate to the requirements of the Business Management System and the Standard are reviewed and approved for suitability by authorized personnel prior to use or release. Where use is made of computer-based documents and files, special attention is paid to appropriate approval, access, distribution, and archiving procedures.

Currently Mythical True Value Metrology has a mixed media documentation system for documents within its control:

-  Levels 1 and 2 are computer based, and are available as read-only documents.

-  Level 3 documents (such as work instructions, standard operating procedures, or calibration procedures) are also in electronic format wherever possible.

-  For the remaining Level 3 documents not in electronic format, authorized distribution to points of use is in hard copy. This includes many documents of external origin.

All electronic QMS documents are stored in protected folders. Many are further protected from unauthorized change by use of digital signatures that implement the RSA public key encryption algorithm. Update access privileges to these folders are limited to the designated document control administrator.

A list of documents, identifying the current revision status of each, is established and is readily available. Appropriate documents are available at all locations where operations essential to the effective functioning of the Business Management System are performed, or they may be readily obtained.

Document and Data Changes 
^^^^^^^^^^^^^^^^^^^^^^^^^

All personnel are required to identify required changes in documents and are encouraged to suggest improvements. Changes to documents and data are reviewed and approved by the same function/organizations that performed the original review and approval, unless specifically designated otherwise. Access to appropriate background information is provided. The nature of the changes is recorded.

When practical, the changes are highlighted in the document and/or on attachments.

Obsolete Documents
^^^^^^^^^^^^^^^^^^

Electronically maintained documents are promptly removed from active files when they become obsolete. They may be maintained for the document retention period, in file areas not accessible to general users.

After a revision issue, obsolete hard copy documents are promptly removed from points of issue and use. The obsolete master document is archived and may be retained for legal and/or knowledge preservation purposes. The obsolete point of use copy is destroyed.

.. note::

    Due to the nature of work performed by Mythical True Value Metrology (calibration and repair of precision tools, inspection, measuring and test equipment) there may be multiple versions of Level 3 documents for a given manufacturer/model of equipment. This is an acceptable situation because: it is due to version changes during production by the manufacturer of the equipment being calibrated and is outside the control of Mythical True Value Metrology; and the metrology technicians are responsible for selecting the correct document version based on the specific version or serial number of the item being calibrated.

    These documents are also exempt from the requirement to destroy old copies, unless and until it can be demonstrated that there will never again be a requirement to calibrate or repair that specific instrument from any current or potential future customer. The primary reason for this exemption is that some of the workload items may be several decades beyond their manufacturer's support life and the data owned by Mythical True Value Metrology may well be the only data available anywhere.

Documents of External Origin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Due to the nature of the organization as a department of a large corporation, there are a number of documents of external origin that we must use and follow but which are controlled elsewhere. Access to and use of these documents is in accordance with applicable Mythical Airlines policies.

Mythical True Value Metrology is also subject to various government regulations. When there is a need to refer to any of these regulations, access is gained through the Mythical Airlines system or the regulatory agency system. Our presumption is that the link from that site goes to the most current edition of the regulation. Any other copies of these regulations are not authorized for use.

These and other situations are further documented in QP 4230.

Control of Quality Records
~~~~~~~~~~~~~~~~~~~~~~~~~~

Quality records are a special case of quality documents. For example, a blank form is a document, a completed form is a record.

Mythical True Value Metrology maintains quality records to demonstrate conformance to specified requirements and the effective operation of the Business Management System. Control provides for the identification, collection, indexing, filing, access, storage, maintenance, and disposition of quality records. Pertinent customer or supplier records are an element of these data.

All quality records are legible and are stored and retained in such a manner that they are readily retrievable. Storage facilities provide an environment that minimizes damage, deterioration and prevents loss. Retention times of quality records are established and recorded. A records control matrix is documented to aid control. QP 4240 is established to implement the above policies and to control Business Management System records.

MANAGEMENT RESPONSIBILITY
=========================

Management Commitment
---------------------

The top management of Mythical True Value Metrology is committed to the development and improvement of an effective Quality Management System. This commitment is demonstrated by:

-  Communicating to the organization the importance of meeting all customer, regulatory, and legal requirements. (:ref:`docs/quality-manual:Internal Communications`)

-  Establishing the Mythical True Value Metrology Quality Policy for total commitment to excellence and the associated quality objectives. The Quality Policy and the Quality Objectives are published and displayed on the Laboratory network sign-in page and the office notice board. (Sections 5.3 Quality Policy and 5.4.1 Quality Objectives.)

-  Conducting Management Reviews. (:ref:`docs/quality-manual:Management Review`)

-  Identifying and acquisition of controls, processes, equipment, fixtures, resources, and skills need to achieve the required quality. The associated quality management activities are described in :ref:`docs/quality-manual:Resource Management`.

Customer Focus
--------------

Top management of Mythical True Value Metrology ensures that customer needs and expectations are determined, converted into requirements, and fulfilled with the aim of meeting or exceeding those expectations. The general needs of our customers are defined by the fact that Mythical True Value Metrology was established by the parent company to perform the specific service of metrology. The specific needs of our customers are determined through continual verbal and written communications, as well as periodic visits to their facilities to better understand their individual processes. It is the responsibility of all personnel that interface with customers to access stated or implied needs and bring them to the attention of top management so that they may be addressed. To continually improve, the department also monitors, measures, and analyzes customer satisfaction throughout the business cycle.

The department also meets all of its other obligations including regulatory and legal requirements. This is further described in :ref:`docs/quality-manual:Determination of Requirements Related to the Equipment`.

Quality Policy
--------------

The top management of Mythical True Value Metrology has defined and documented its Quality Policy. This policy includes the organization’s commitment for meeting customer requirements and to continual improvement.

**Quality Policy Statement:**

-  The Quality Policy of Mythical True Value Metrology is to provide high and consistent quality in the calibration and servicing of precision tools, inspection, and measuring and test equipment owned by Mythical Airlines and other customers. We are always mindful of the fact that the items we calibrate and repair are used in maintenance of aircraft and all of their systems, an environment where safety is paramount.

-  Our commitment is to the safety and accurate work of our customer's professionals who use the items we have calibrated, thereby ensuring the safety and comfort of aviation passengers and other customers.

-  All work is done in conformance to Mythical True Value Metrology’s QMS, the applicable technical and administrative operating policies and procedures of Mythical Airlines, legal and regulatory requirements, and specific customer requirements.

-  Through front-line input and management leadership, we will continue to improve our people and processes to anticipate, meet, and exceed the needs of our customers.

-  We support the continually improving quality of our customer's maintenance and other technical operations through the the services we provide.

The quality policy and quality objectives are relevant to the department’s organizational goals and the expectations and needs of its customers. These concepts have been communicated to all employees within the scope of the QMS. Quality concepts have been integrated into Mythical True Value Metrology’s culture.

The top management of Mythical True Value Metrology ensures that its Quality Policy is understood. This is accomplished through training, communication, and displays. The Policy is implemented by means of documentation, and training about the Business Management System to meet the requirements of the Standard. The Policy is maintained through Business Management Systems audits and corrective and preventive action, and reviewed for continuing suitability through periodic Management Reviews. Further, this Quality Policy is supported by the quality objectives listed in this Business Operating Manual. (:ref:`docs/quality-manual:Quality Objectives`).

Planning
--------

Quality Objectives 
~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology establishes quality objectives at all relevant functions and levels within the organization. The objectives are derived from Mythical True Value Metrology’s Quality Policy and Mythical Airlines corporate policies. The quality objectives are documented (Annual Quality Objectives) and are reviewed by top management.

The quality objectives are measurable and consistent with the quality policy, including commitment to continual improvement. Quality objectives include those needed to meet all contracted requirements. Inherent in the entire set of quality objectives is a focus on customer satisfaction.

Quality Management System Planning 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology has identified and planned the resources needed to achieve its quality objectives. The results are documented in management reviews. Quality planning addresses continual improvement of the QMS and assures that change is conducted in a controlled manner so that the integrity of the QMS is maintained during this change.

The Quality Plan identifies the core and support business processes of the QMS, and is documented in the form of references to associated Quality Procedures. The Core Business Processes represent how we turn opportunity into profitable outcomes. Its effectiveness is maintained and enhanced by Support Processes. The identified Core and Support business processes are indicated in :numref:`Fig. {number} <core-support-business-processes>`

.. figure:: figures/core-support-business-processes.png
    :width: 850px
    :name: core-support-business-processes
    
    Core support business processes
    
Each core and support business process has a process owner who has the prime responsibility for ensuring that the process:

-  achieves its objectives, and

-  is under continual review for improvement.

The business processes, their associated procedures, and the business process owners are shown in the Business Process and Responsibility Matrix. Each of the core business processes and support processes is linked to a specific Level 2 quality procedure.

The Quality Plan describes the overall plan for quality assurance. The quality plan consists of

-  the core business processes and their interactions,

-  the support processes and their interactions,

-  the Business Process and Responsibility matrix, and

-  appropriate outputs of management review, such as supporting resources.

The Quality Plan is assessed for on-going suitability and effectiveness during management review meetings.

Responsibility, Authority and Communication
-------------------------------------------

Responsibility and Authority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology has defined business functions and their interrelations within the organization, including responsibilities and authorities; and those are communicated in order to facilitate effective quality management. For the purpose of this Business Operating Manual and the quality management system, **top management of Mythical True Value Metrology is the Vice-President — Quality of Mythical Airlines**.

Mythical True Value Metrology ensures the responsibility, authority and interrelationship of personnel who manage, perform, and verify work affecting quality is defined and documented, particularly for personnel who need the organizational freedom and authority to:

-  Initiate action to prevent the occurrence of any nonconformance relating to service, process, and Business Management Systems,

-  Identify and record any problems relating to the service, process, and Business Management System,

-  Initiate, recommend, or provide solutions through designated channels,

-  Verify the implementation of solutions,

-  Control further processing, delivery or installation of nonconforming product until the deficiency or unsatisfactory condition has been corrected.

The Mythical True Value Metrology organization chart :numref:`Fig. {number} <organization-and-top-management>` shows the organization of the department and the relationships to the Mythical Airlines corporate management. Relationships to other parts of the Mythical Airlines corporate structure are documented by Mythical Airlines and are available on the Mythical Airlines network. In the organization chart, the portion INCLUDED in this quality management system is shaded in blue. The rest of the company is treated as customers or suppliers, as appropriate.

Management Representative
~~~~~~~~~~~~~~~~~~~~~~~~~

The top management of Mythical True Value Metrology has appointed the Mythical Airlines Director – Metrology as its Management Representative. This individual, irrespective of other responsibilities, has the defined authority to:

-  Ensure that the business processes of the QMS are defined.

-  Ensure that the Business Management System requirements are established, implemented and maintained in accordance with the Standard.

-  Report to top management on the performance of the QMS, including needs for improvement.

-  Promote awareness of customer requirements throughout the organization.

The Management Representative also acts as the liaison between the department and other third parties on matters concerning the department Business Management System.

The Metrology Engineering and Support Manager is the Deputy Management Representative.

.. figure:: figures/organization-and-top-management.png
    :width: 850px
    :name: organization-and-top-management
    
    Organization and top management
    
Internal Communications 
~~~~~~~~~~~~~~~~~~~~~~~

Top management of Mythical True Value Metrology ensures that communication is maintained between its various levels and functions regarding the processes of the QMS and their effectiveness. This is accomplished via the Laboratory network, various quality meetings, and other internal publications.

Management Review
-----------------

Top management of Mythical True Value Metrology reviews the QMS at planned intervals to ensure its continuing suitability, adequacy and effectiveness. The review evaluates the need for changes to the organization's QMS, including its quality policy and quality objectives.

Review Input
~~~~~~~~~~~~

Inputs to management review include but are not limited to current performance and improvement opportunities related to the following:

-  results of audits;

-  customer feedback;

-  process performance and product conformance;

-  status of preventive and corrective actions;

-  follow-up actions from earlier management reviews;

-  changes that could effect the QMS; and

-  recommendations for improvement of the system.

Review Output
~~~~~~~~~~~~~

The outputs from the management review include but are not limited to decisions and actions related to:

-  improvement of effectiveness of the QMS and its processes;

-  improvement of product related to customer requirements; and

-  resource needs.

Results of management reviews are recorded.

RESOURCE MANAGEMENT
===================

Provision of Resources
----------------------

Top management of Mythical True Value Metrology determines and provides, in a timely manner, the resources needed:

-  to implement, maintain and improve the effective operations of the QMS processes, and

-  to enhance customer satisfaction by meeting requirements.

These resources are assessed and reviewed on a periodic basis consistent with annual and strategic business planning activities.

Human Resources
---------------

Assignment of Personnel
~~~~~~~~~~~~~~~~~~~~~~~

Personnel who are assigned responsibilities directly or indirectly affecting conformity to service (product) requirements are determined to be qualified and competent based on education, training, observed skills, and experience.

Competence, Awareness, and Training
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies training needs and provides for the training of all personnel performing activities affecting quality. Personnel performing specific assigned tasks are qualified on the basis of appropriate education, training, observed skills, and experience, as required. All metrology technicians must be evaluated as fully competent in a discipline of metrology before being allowed to work independently in that discipline.

A training plan is prepared and scheduled annually with periodic updates.

Training is provided either on or off the job, internally or externally, as appropriate. The ongoing effectiveness of training is periodically assessed by observation, proficiency testing or other appropriate means.

Mythical True Value Metrology ensures that its personnel are aware of the relevance and importance of their jobs and how they contribute to the achievement of the quality objectives.

Mythical True Value Metrology securely retains duplicate copies of training records in the form of attendance sheets, certificates of proficiency and/or completion for all employees. Original copies of these records are maintained by Mythical Airlines Human Resources Department, which is outside the scope of this quality management system.

Quality Procedure QP 6220 implements the above policies.

Infrastructure
--------------

Top management of Mythical True Value Metrology identifies, provides and maintains the facilities and infrastructure it needs to achieve service quality, including:

-  workspace and associated facilities, including utilities;

-  process equipment, hardware and software;

-  supporting services such as telephone, computer networks, etc.

QP 6300 and QP 6301 have been established to further describe and implement the above policies.

Work Environment 
----------------

Top management of Mythical True Value Metrology identifies and manages the human and physical factors of the work environment needed to achieve conformity of service quality.

QP 6400 has been established to further describe and manage the above policies, and to define the physical environmental requirements for each metrology discipline.

SERVICE REALIZATION
===================

.. note:: The product of Mythical True Value Metrology is the service of providing calibration of tools, precision measuring equipment, and testing equipment.

Planning of Service Realization 
-------------------------------

Upon receipt of new service requirements, or significant changes (modify, add, remove) to existing requirements, Mythical True Value Metrology management plans the sequence of processes required to realize the specified requirements. Planning the processes for the realization of service quality includes but is not limited to the functions listed below.

-  Prepare documentation and data that describes how the processes of the QMS are applied to a specific service. These are referred to as calibration procedures or calibration documents. Such documents describe the quality objectives and requirements, the sequence of processes and sub-processes required to achieve the contracted services including specified performance objectives. Quality plans are consistent with the other requirements of the organization's QMS and are documented in a form suitable for the department’s method of operation.

-  Identify and document suitable verification, validation, monitoring, process measurement, inspection, and testing at appropriate stages of the calibration service realization.

-  Identify and acquire controls, processes, equipment, fixtures, total calibration resources, and skills that are needed to achieve the required quality.

-  Address for compatibility the delivery of service, calibration process, installation, servicing, inspection and test procedures, and the applicable documentation.

-  Address the need to develop and implement new techniques, and the criteria for acceptance, as necessary.

-  Address measurement requirements involving capability that exceeds the known state of the art.

-  Clarify standards of acceptability, including those that contain a subjective element.

-  Identify quality records that are necessary to provide confidence of conformity of the processes and results, and the records are maintained as described in documented procedures.

Quality Procedure QP 7100 describes the planning for service delivery and refers to other Mythical True Value Metrology quality procedures involved in the process.

Customer-Related Processes
--------------------------

Determination of Requirements Related to the Equipment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology determines customer requirements including service and/or product requirements specified by the customer. These customer requirements include, but are not limited to:

-  specific calibration requirements;

-  the requirements for availability, and delivery, with associated costs;

-  contract requirements not specified by the customer but necessary for intended or specified use; and

-  obligations related to the contract, including regulatory and legal requirements.

.. note:: Mythical True Value Metrology is established by Mythical Airlines (its principal customer) to provide calibration and related services. Contract review or other customer determination processes are not an on-going requirement with respect to the calibration service that we provide to Mythical Airlines. If equipment is delivered to us using Mythical Airlines’ processes and no exceptions are noted, the customer and we expect that routine calibration service will be provided with no need for further determination or review.

Quality Procedure QP 7200 is established to implement and provide further guidance regarding customer-related processes.

Review of Requirements Related to the Product
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If equipment is delivered to us using Mythical Airlines’ processes and no exceptions are noted, the customer and we expect that routine calibration service will be provided with no need for further determination or review. Should that expectation change due to a change in Mythical Airlines policy, it will be reviewed and documented at management review.

Mythical True Value Metrology ensures that customer requirements are fully understood and the department has the capability to meet them. Before acceptance of a non-routine customer requirement, including work from customers other than Mythical Airlines, the request is reviewed by the department to ensure that:

-  Requirements are adequately defined and documented.

-  Differences between the standard requirements and those in the request are resolved.

-  Mythical True Value Metrology has the capability and capacity to meet these requirements.

If a change to the standard requirements is identified by the laboratory or requested by the customer, the department documents such statements of or changes to requirements. The department ensures that relevant documentation is amended and relevant personnel are made aware of the changed requirements.

Customer Communication
~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies and implements arrangements for communication with customers relating to:

-  calibration information;

-  inquiries, or order handling, including amendments;

-  customer feedback, including customer complaints.

DESIGN AND DEVELOPMENT
----------------------

Mythical True Value Metrology is not currently engaged in the design or development of product. Mythical True Value Metrology does not design or develop the equipment or service used in the calibration process, or the equipment that is calibrated. The equipment calibration is based on verifiable performance specifications and requirements of the equipment manufacturer and/or the customer. Work instructions (calibration procedures) are either publicly available methods or methods written by the metrology engineering staff based on publicly available methods. In either case, the methods are verified and validated according to generally acceptable metrology principles before being accepted for use. Therefore, clause 7.3 of the ISO 9001 standard is not currently applicable. Should the department become involved in product design, then it will define and document suitable controls to ensure conformance to specified requirements.

PURCHASING
----------

Purchasing of outside materials, supplies, and services is not a burdensome requirement for Mythical True Value Metrology. In most cases, Mythical Airlines is considered the supplier of all purchasing services.

-  All purchases of capital items, high volume consumables, items or services requiring a purchase order, contracted services, and so on are largely handled by other Mythical Airlines departments and are under the control of Mythical Airlines’ policies and procedures and therefore outside the scope of this QMS.

-  Much of the purchasing process uses Mythical Airlines requirements, controls and staff, which are outside the scope and controls of the Mythical True Value Metrology QMS.

-  Suppliers that may be used with a purchase order or through the electronic business-to-business system are controlled by the Mythical Airlines corporate purchasing and accounting departments, which are outside the scope of this QMS.

-  Some materials and supplies are obtained from the Mythical Airlines stock system.

-  Support processes are supplied and controlled by other Mythical Airlines departments.

Mythical True Value Metrology directly purchases some materials, supplies, and services from other companies by using Mythical Airlines’ corporate purchase card. Almost all such purchases are infrequent or non-recurring retail purchases of very small quantities of cataloged or other commercial off-the-shelf products. All purchases must follow the policies and procedures of Mythical Airlines, which are outside the scope of this QMS.

Mythical True Value Metrology maintains a local list of active outside (non-Mythical Airlines) suppliers and the materials or services they provide, but suppliers are primarily managed at the corporate level and must be listed in the corporate enterprise management system database.

Mythical Airlines provides for the inspection and auditing of suppliers, when a need is determined, according to Mythical Airlines policy. In this respect Mythical Airlines is a supplier of this service to Mythical True Value Metrology.

Mythical True Value Metrology, where practical, verifies purchased material, supplies, and services before use or redistribution.

Purchasing Process
~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology ensures that purchased products and services conform to specification and are purchased from approved sources. Purchasing control is dependent on the type of product, on the effect on subsequent service realization processes and their output, and where applicable, on the quality audit reports and/or quality records of suppliers’ previously demonstrated capability and performance. In all cases, incoming product is not used until it is inspected or otherwise verified against specified requirements.

Mythical True Value Metrology has established and maintains local records of acceptable suppliers. An approved supplier list (ASL) is maintained. A critical supplier list is also maintained.

QP 7400 defines the Mythical True Value Metrology process for purchasing controls over its suppliers. Separate procedures detail the processes for purchasing and materials, supplies and services other than calibration (QP 7410.1), and subcontracted calibration services (QP 7410.2).

Purchasing Information
~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology requires that purchasing documents contain data clearly describing the product ordered including, where appropriate, requirements for approval or qualification of

-  product and/or services,

-  procedures,

-  processes,

-  equipment, and

-  personnel

All appropriate and relevant contract clauses have been supplied to Mythical Airlines purchasing department, and all requests for external services or supplies identify the relevant clauses to be used. All purchasing documents are reviewed and approved to ensure the adequacy of specified requirements contained in the purchasing documents prior to their release.

Verification of Purchased Product
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies and implements the activities necessary for verification of purchased products and/or services to ensure that purchase specifications are met.

Mythical True Value Metrology ensures that incoming material is not used or processed until it has been inspected or otherwise verified to specified requirements. Verification of conformance is in accordance with the quality plan and/or documented procedures.

Mythical True Value Metrology has determined the amount and nature of receiving verification, based on the extent of control exercised at the supplier premises and the recorded evidence of conformance provided.

Verification of purchased product or service by Mythical True Value Metrology at the supplier’s premises is not required at this time. If it ever does become a requirement, this process will be re-evaluated.

Verification of purchased product or service by the Customer is not required at this time. Such verification would required recalibration of the instrument, and the customer does not have the facilities, measurement standards, or qualified personnel to perform the verification. If it ever does become a requirement, this process will be re-evaluated. However, this does not absolve Mythical True Value Metrology of the responsibility to provide acceptable product, nor will it preclude subsequent rejection by the customer.

CALIBRATION AND SERVICE PROVISION
---------------------------------

Control of Production and Service Provision
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies and plans calibration processes that directly affect quality to ensure that these processes are carried out under controlled conditions. The plan is modified, if necessary, based on review of new customer requirements.

These plans are executed under controlled conditions. Controlled conditions include the following:

-  The availability of information that specifies the product/calibration characteristics.

-  Use of documented work instructions defining the manner of calibration, where the absence of such instructions could adversely affect quality.

-  Use of suitable calibration equipment in a suitable work environment.

-  Compliance with reference standards, codes, control plans, and/or documented procedures and work instructions. Calibration personnel have access to appropriate product specifications information and control measures at each stage of the process.

-  Monitoring and control of suitable product characteristics and process parameters, and the availability and use of measuring and monitoring devices.

-  The approval of process and equipment as required.

-  Criteria for workmanship are stipulated in the clearest practical terms using written standards, representative samples, or illustrations, as appropriate.

-  Suitable maintenance of equipment to ensure continuing process capability.

-  The implementation of defined processes for release, delivery, and any applicable post-delivery activities.

Validation of Processes for Calibration Provision
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Work processes (calibration procedures) are validated before first use and periodically as necessary as determined by Metrology Engineering, proficiency test results, observed deficiencies, or measurement system changes. The nature of a calibration service is that all of the customer's product is either verified to be in good working order before release, or rejected as unusable.

The need for special processes is confirmed or ruled out during product planning. See Quality Procedure QP 7100.

If applicable, Mythical True Value Metrology will validate any calibration and service processes where the resulting output cannot be verified by subsequent measuring or monitoring. This includes any processes where deficiencies may become apparent only after the equipment is in use or the service has been delivered. Currently there are no special processes in use by Mythical True Value Metrology where the results cannot be fully verified by subsequent inspection or testing.

Validation demonstrates the ability of the processes to achieve planned results. Mythical True Value Metrology defines arrangements for validation, including the following, as applicable:

-  qualification of processes;

-  qualification of equipment and personnel;

-  use of defined methodologies and procedures;

-  requirements for records;

-  re-validation.

Identification and Traceability
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology ensures that equipment in a work process is adequately identified by suitable means throughout the process. The department also provides for physical traceability (identification of location and step in the process).

Identification
^^^^^^^^^^^^^^

Equipment is adequately identified through all stages by means of a Mythical Airlines property ID number, or the property ID number provided by an external customer, as applicable.

Traceability
^^^^^^^^^^^^

When and to the extent that physical traceability is a specified requirement, Mythical True Value Metrology will establish and maintain documented procedures for the unique identification of product.

.. note:: Traceability in this sense refers to physically tracking a product through the service provision process and not to metrological traceability of measurement results.

Inspection and Test Status
^^^^^^^^^^^^^^^^^^^^^^^^^^

Mythical True Value Metrology identifies the status of the equipment with respect to measurement and monitoring requirements specified. The identification of inspection and test status is maintained throughout the process to ensure that only equipment that has passed required inspections, tests, and calibration are dispatched or used as serviceable items.

-  Suitable means of identification are in place throughout the calibration process.

-  Computer records are the primary means of identification of status.

-  Job numbers, tags, labels, inspection logs, traveler sheets, and so forth may also be used when appropriate.

-  Since all operations are performed on unique single items, any method relating to a lot of more than one item is by definition not appropriate.

Customer Property
~~~~~~~~~~~~~~~~~

Mythical True Value Metrology exercises care with customer property while it is under our control or being used by the organization. The department identifies, verifies, protects, and maintains customer property provided for service. Any occurrence of customer property that is lost, damaged, or otherwise found to be unsuitable for use is recorded and reported to the customer for disposition.

Customer property includes intellectual property (information provided in confidence) and proprietary information. All personnel are responsible for following Mythical Airlines' general corporate policies on confidentiality. These policies apply equally to intellectual property of external customers.

Preservation of Product
~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology preserves conformity of equipment with customer requirements during internal processing and delivery to the intended destination. This includes identification, handling, packaging, storage, and protection. This also applies to the constituent parts of and accessories for a product.

Quality Procedure QP 7550 provides for the preservation of equipment quality through all stages.

Control of Monitoring and Measuring Devices
-------------------------------------------

.. note::

    This section applies to the calibration and maintenance of Mythical True Value Metrology’s own equipment used to provide its calibration services.

    In this respect, Measuring and Monitoring Devices may also be referred to as measurement standards, calibration standards, or laboratory standards. All of these terms are equivalent.

Mythical True Value Metrology identifies the measurements to be made and selects the measuring and monitoring devices required to assure conformity of calibration to specified requirements.

Adequately calibrated measuring and monitoring devices (measurement standards) are used and controlled to provide assurance that measurement capability is consistent with the measurement requirements, and that measurement results are traceable to the International System of Units.

Where applicable, Mythical True Value Metrology ensures that measuring and monitoring devices:

-  are calibrated on a defined periodic basis and adjusted when the need is indicated, against devices traceable to nationally recognized standards. Where no such standards exist, the basis used for calibration is agreed upon and recorded;

-  are safeguarded from adjustments that would invalidate the calibration;

-  are protected from damage and deterioration during handling, maintenance, and storage;

-  calibration results are recorded;

-  have the validity of previous results re-assessed if measuring and monitoring devices are subsequently found to be out of calibration, and corrective action taken.

Where software is used to control measuring and monitoring of specified requirements, it is validated prior to initial use. Once the software has been verified to function as intended (as part of the receiving inspection process), then no further tests are required unless a version upgrade is installed or a skilled operator has reason to suspect a problem.

Quality Procedure QP 7600 provides additional guidance on control of measurement standards and implements this policy.

Substitution of Test and Measurement Equipment Used as Measurement Standards
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When the measurement standard called for in the calibration procedure does not exist or is not available on a more or less permanent basis, a substitution may be required. The substitute equipment may also be referred to as “equivalent” or “alternate” equipment.

When the calibration procedure used is documented in an aircraft, engine or component maintenance document controlled by the controlled maintenance manual procedures of Mythical Airlines, or an Airworthiness Directive, any substitute for the measurement standard must be requested and approved through Mythical Airlines' corporate Tooling Equivalency process. The work cannot be accomplished until approval is documented and received.

In all other cases, a substitute standard request must be submitted to the metrology engineering staff for approval and documentation.

MEASUREMENT, ANALYSIS & IMPROVEMENT
===================================

Mythical True Value Metrology defines, plans, and implements the measurement and monitoring activities needed to ensure conformity and achieve improvement. This includes the determination of the need for, and use of, applicable methodologies including statistical techniques.

Monitoring and Measurement 
--------------------------

Customer Satisfaction
~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology monitors information on customer satisfaction and/or dissatisfaction as one of the measurements of performance of the QMS. The quality manager determines the methods for obtaining and using this information.

Internal Audit
~~~~~~~~~~~~~~

Mythical True Value Metrology conducts periodic internal audits, or schedules equivalent audits by other Mythical Airlines departments or by external contractors. The purpose of the internal audit is to determine whether the department's QMS:

-  conforms to the requirements of the Standard;

-  is being effectively implemented and maintained.

Internal quality audits are planned and scheduled on the basis of the status and importance of the activity to be audited. Trained personnel independent of those having direct responsibility for the activity being audited carry out the audits, thus ensuring that auditors do not audit their own work. Since Mythical True Value Metrology is so small that independence cannot always be fully assured, the department may arrange for the internal audit function to be performed by other qualified parties. Examples of other qualified parties include but are not limited to:

-  Agents of the customer, such as Mythical Airlines' own internal quality audit department.

-  Independent third-party organizations, such as a qualified consultant or consulting firm.

-  Qualified internal auditors from other departments of Mythical Airlines.

Mythical True Value Metrology currently schedules internal audits that are performed by Mythical Airlines' own internal quality audit department.

The results of the internal audits are documented and brought to the attention of the personnel having responsibility for the area audited. Management personnel responsible for the area take timely corrective action on the deficiencies found during the audit.

Follow-up activities verify and record the implementation of the corrective action, report the verification results, and close out the audit. Subsequent audits verify the effectiveness of the corrective actions taken.

Results of internal audits and the corrective action are submitted for management review.

QP 8220 implements this policy.

Monitoring and Measurement of Processes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology applies suitable methods for measurement and monitoring of the realization processes necessary to meet customer requirements. These methods confirm the continuing ability of each process to satisfy its intended purpose. Suitable methods are determined by Metrology Engineering and may include process control checks, proficiency tests, and other methods.

Monitoring and Measurement of Product
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The purpose of Mythical True Value Metrology is to measure and monitor the characteristics of the equipment being calibrated to verify that requirements are met. This is carried out at appropriate stages by the calibration process and may include equipment testing and/or inspection as required by the quality plan and/or documented work instructions.

Evidence of conformity with the acceptance criteria is documented. Records indicate the authority responsible for release of product.

Mythical True Value Metrology carries out final inspection in accordance with the quality plan and/or documented procedures to complete the evidence of conformance of the finished product/service to specified requirements. See QP 4106.

The quality plan and/or documented procedures require that all specified inspections and tests and calibrations, including those specified on receipt or in process, have been carried out and that the results meet specified requirements.

Equipment release and service delivery does not proceed until all the specified activities have been completed and the associated data and documentation are available and authorized unless otherwise approved by the customer.

Mythical True Value Metrology has established and maintains records that provide evidence that equipment has been inspected and/or tested and calibrated. These records show clearly whether the equipment has passed or failed according to defined acceptance criteria. Where the equipment fails to pass any inspection, the process defined in the applicable Mythical Airlines policy (outside the scope of this Business Management System) is followed. In the case of equipment owned by an external customer, the customer is notified and the item is returned as rejected with the specific deficiencies noted.

Records identify the calibration authority responsible for the release of the equipment.

Control of Nonconforming Product
--------------------------------

.. note:: As noted in sections 2.2 and 7 of this Business Operating Manual, the "product" of this organization is the service of performing calibration of precision electronic test and measuring instruments.

Mythical True Value Metrology ensures that service provided which does not meet requirements is identified, controlled where possible to prevent unintended use or delivery to the customer, and corrected if it has been delivered. This policy is implemented in QP 8300. This procedure includes provisions for:

-  identification, documentation, evaluation, segregation (where practical), disposition of nonconforming service, and for notification of the functions concerned;

-  assigning responsibility for the review and the authority for disposition of nonconforming service;

-  correction of nonconforming service and re-verification/calibration of the affected equipment after correction to demonstrate conformity (if necessary);

-  handling of nonconforming service when it is detected after delivery to the customer.

Analysis of Data 
----------------

Mythical True Value Metrology collects and analyzes appropriate data to determine the suitability and effectiveness of the QMS and to identify improvements that can be made. This includes data generated by measuring and monitoring activities and other relevant sources.

The data are analyzed to provide information on:

-  customer satisfaction and/or dissatisfaction;

-  conformance to customer requirements;

-  characteristics of processes, product and their trends;

-  performance of suppliers.

Improvement
-----------

Continual Improvement
~~~~~~~~~~~~~~~~~~~~~

Mythical True Value Metrology plans and manages the processes necessary for the continual improvement of the QMS.

Mythical True Value Metrology facilitates the continual improvement of the QMS through the use of the quality policy, objectives, audit results, analysis of data, corrective and preventive action and management review.

Corrective Action
~~~~~~~~~~~~~~~~~

Mythical True Value Metrology takes corrective action to eliminate the cause of identified nonconformities in order to prevent recurrence. Corrective actions are appropriate to the impact of the problems encountered.

QP 8520 defines requirements for:

-  identifying nonconformities (including customer complaints);

-  determining the causes of nonconformity;

-  evaluating of the need for actions to ensure that nonconformities do not recur;

-  determining and implementing the corrective actions needed;

-  recording results of action taken;

-  review and evaluation of corrective action taken to assess its effectiveness.

Preventive Action
~~~~~~~~~~~~~~~~~

Mythical True Value Metrology identifies preventive actions to eliminate the causes of identified potential nonconformities to prevent initial occurrence. Appropriate sources of information such as processes and work operations results which affect product quality, concessions, audit results, quality records, service reports, and customer complaints are analyzed (see 8.4 above) to detect preventive action possibilities. Preventive actions taken are appropriate to the impact of the potential problems.

QP 8530 defines requirements for:

-  identification of potential nonconformities and their causes;

-  determination of the steps needed to eliminate identified causes and completion of the preventive action implementation;

-  recording results of action taken;

-  review and evaluation of preventive action taken to assess its effectiveness;

-  ensuring that relevant information on actions taken, including changes to procedures, is subject to management review.

**- - - [END] - - -**

.. |image0| image:: media/image2.wmf
   :width: 6.49861in
   :height: 5.30417in
.. |image1| image:: media/image3.wmf
   :width: 5.78819in
   :height: 5.06736in
.. |image2| image:: media/image4.wmf
   :width: 4.88819in
   :height: 5.90833in
