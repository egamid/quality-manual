
.. only:: latex

    ********
    Contents
    ********

    .. toctree::
      :maxdepth: 3
      :caption: Contents

    .. raw:: latex
       
       \let\cleardoublepage\clearpage
       \textsf{\listoffigures}
       \let\clearpage\cleardoublepage
       \clearpage
       
.. warning:: 
    
    This is document was generated as-is as an example of docs-as-code workflow and capabilities. Its content is not accurate or checked. 
    
    This EXAMPLE quality manual was created for ASQ by: 
    
    Graeme C. Payne
    
    ASQ Senior Member, CQA, CCT, CQE, CQT
    
    GK Systems, Inc. 
    4440 Weston Drive SW, Suite B
    Lilburn GA 30047-3181
    USA
    http://www.gksystems.biz 
    
    This EXAMPLE manual is a representative item for use as a training aid only. Any resemblance to any real corporation, organization, policies or procedures is not intentional and is purely coincidental. 

**Mythical True Value Metrology**

*a department of*

**Mythical Airlines**

**Legendary Safety, Service, Speed and Strength Around the World from**

**Smallville, Kansas, USA**

**BUSINESS OPERATING MANUAL**

(QUALITY MANUAL)

**Revision Date:** 2019-09-23

**Approved By:** Director, Mythical True Value Metrology Date: 2019-09-23

*This document is authenticated in whole by verification of the above digital signature, using the signer's public key.*

   This is a complete re-issue of the manual with all previous changes incorporated, therefore there are no revision marks. This version replaces all prior issues.

   This manual is the property of Mythical True Value Metrology. It must not be reproduced in whole or in part or otherwise disclosed without prior written consent from Mythical True Value Metrology.

   The official controlled copy of this manual is the digitally signed PDF document on the Mythical True Value Metrology network server and visible to all authorized users. All printed copies, and all electronic copies and versions except the ones described above, are considered uncontrolled copies used for reference only.

   This document is controlled as a single entity, as any change – however slight, even a single character – to any part of the document by definition changes the entire document. For this reason, as well as the fact that the concept of “page” varies with the publication format, page-level revision is not practiced with this or any other Mythical True Value Metrology document.
   
.. toctree::
    :maxdepth: 3
    :numbered:

    topics/introduction
    topics/distribution-and-review
    topics/quality-management-system
    topics/management-responsibility
    topics/resource-management
    topics/service-realization
    topics/measurement-analysis-and-improvement
