#!/bin/sh
for f in docs/*/*.rst; do aspell -x -d, --master=american --mode=html check $f; done
for f in shared/*.rst; do aspell -x -d, --master=american --mode=html check $f; done
for f in shared/*.txt; do aspell -x -d, --master=american --mode=html check $f; done
for f in docs/*/tables/*.csv; do aspell -x -d, --master=american --mode=html check $f; done
for f in _static/conf-files/*/conf.py; do aspell -x -d, --master=american --mode=html check $f; done
for f in *.rst; do aspell -x -d, --master=american --mode=html check $f; done
for f in conf.py; do aspell -x -d, --master=american --mode=html check $f; done
for f in *.md; do aspell -x -d, --master=american --mode=html check $f; done

#windows
for %f in (*.rst) do aspell -x -d, --master=american -c %f