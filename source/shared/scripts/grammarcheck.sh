#!/bin/sh

echo 'Passive and complex sentences, readability report:'; done
echo; done

style -p -r 25 docs/*/*.rst docs/*.rst shared/*.* docs/*/tables/*.csv

echo; done
echo 'Aim for a Flesch index score over 65 and less than 10% long / passive sentences.'; done
