Go to [Contributing to documentation wiki](https://gitlab.com/demo-docs-as-code/qms/wi-cms/wikis/contributing-to-documentation) to see all the information on how to update technical document repositories.

Use the below branch names. See [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) for more information. There are two key branches that hold the content: *master* and *develop*. The *develop* branch is for updating content and collaboration. Once the content is ready, it can be merged onto the *master* branch. The *master* branch is our versioned reference for documentation and can only be amended via approval from a gatekeeper.

| Branch  | Role                                                   | Branch off | Merge into         | Naming convention                      |
| ------- | ------------------------------------------------------ | ---------- | ------------------ | -------------------------------------- |
| master  | The versioned and released branch                      | n/a        | n/a                | n/a                                    |
| develop | The working branch for the next release                | master     | n/a                | n/a                                    |