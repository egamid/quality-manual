This is an example documentation repository.

This is document was generated as-is as an example of docs-as-code workflow and capabilities. Its content is not accurate or checked. 

This EXAMPLE quality manual was created for ASQ by: 

Graeme C. Payne

ASQ Senior Member, CQA, CCT, CQE, CQT

GK Systems, Inc. 
4440 Weston Drive SW, Suite B
Lilburn GA 30047-3181
USA
http://www.gksystems.biz 

This EXAMPLE manual is a representative item for use as a training aid only. Any resemblance to any real corporation, organization, policies or procedures is not intentional and is purely coincidental. 

## CMS system

See the guidelines for creating, managing, and distributing technical documentation on the wiki at [Docs-as-Code wiki](https://gitlab.com/demo-docs-as-code/qms/wi-cms/wikis/home).