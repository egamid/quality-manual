# Changelog

All notable changes to this project will be documented in this file as an overview. Specific information should be captured via the workflow defined in *.gitlab/issue_templates/general-issue.md*.

Export details via the csv export function in *Issues*. Filter with the tag *changelog* and any relevant milestones.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).